/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.eval;

import eu.melodic.security.authorization.client.AuthorizationServiceTomcatInterceptor;
import eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Slf4j
@Configuration
public class WebMvcConfig implements WebMvcConfigurer, ApplicationContextAware {
	
	private ApplicationContext applicationContext;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
		log.debug("Got application context");
	}
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		log.info("**********  Registering AuthorizationServiceTomcatInterceptor  **********");
		AuthorizationServiceClientProperties properties = (AuthorizationServiceClientProperties) applicationContext.getBean(AuthorizationServiceClientProperties.class);
		registry
			//.addInterceptor(new AuthorizationServiceTomcatInterceptor( properties ))
			.addInterceptor( AuthorizationServiceTomcatInterceptor.getSingleton( properties ) )
			.addPathPatterns("/test2/**")
			.excludePathPatterns("/test2/no/**");
	}
}