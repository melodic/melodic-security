/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.eval;

import eu.melodic.security.authorization.client.AuthorizationServiceClient;
import eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.BadRequestException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Slf4j
@RestController
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class EvaluationController {
	private AuthorizationServiceClientProperties clientProperties;

	@RequestMapping(value = "/", method = GET)
	public String index() {
		return test(10);
	}
	
	@RequestMapping(value = "/test/{howmany}", method = GET)
	public String test(@PathVariable("howmany") long howmany) {
		log.info("**********  START - test **********");
		log.info("Evaluation iterations: {}", howmany);
		log.info("authorization-properties:\n{}", clientProperties);
		AuthorizationServiceClient client = new AuthorizationServiceClient( clientProperties );
		long cntTotal = 0;
		long cntSuccess = 0;
		long cntFail = 0;
		long tmStart = System.currentTimeMillis();
		
		for (int iter=0; iter < howmany; iter++) {
			try {
				cntTotal++;
				String subject = java.util.UUID.randomUUID().toString();
				String action = java.util.UUID.randomUUID().toString();
				String resource = java.util.UUID.randomUUID().toString();
				client.requestAccess(resource, action, subject);
				cntSuccess++;
			} catch (Exception ex) {
				log.warn("Exception caught: {}", ex);
				cntFail++;
			}
		}
		
		long tmEnd = System.currentTimeMillis();
		long dur = tmEnd - tmStart;
		log.info("Iterations Total:   {}", cntTotal);
		log.info("Iterations Success: {}", cntSuccess);
		log.info("Iterations Fails:   {}", cntFail);
		log.info("Duration: {}ms", dur);
		log.info("Avg. time/iter: {}ms", dur*1.0d/howmany);
		log.info("**********  END - test **********");
		return "Test OK\n"+dur+"ms\n"+cntSuccess;
	}
	
	@RequestMapping(value = "/test-conc/{howmany}/{workers}", method = GET)
	public String test(@PathVariable("howmany") long howmany, @PathVariable("workers") int nWorkers) {
		log.info("**********  START - test-conc **********");
		log.info("Evaluation iterations: {}", howmany);
		log.info("Worker threads:        {}", nWorkers);
		log.info("authorization-properties:\n{}", clientProperties);
		//long cntTotal = 0;
		//long cntSuccess = 0;
		//long cntFail = 0;
		long tmStart = System.currentTimeMillis();
		
		log.info("Starting workers...");
		ExecutorService executor = Executors.newFixedThreadPool(nWorkers);
		List<Future<Long>> list = new ArrayList<Future<Long>>();
		for (int i = 0; i < nWorkers; i++) {
			Callable<Long> worker = new Callable<Long>() {
				public Long call() {
					AuthorizationServiceClient client = new AuthorizationServiceClient( clientProperties );
					long tmWorkerStart = System.currentTimeMillis();
					for (int iter=0; iter < howmany; iter++) {
						try {
							//cntTotal++;
							String subject = java.util.UUID.randomUUID().toString();
							String action = java.util.UUID.randomUUID().toString();
							String resource = java.util.UUID.randomUUID().toString();
							client.requestAccess(resource, action, subject);
							//cntSuccess++;
						} catch (Exception ex) {
							log.warn("Exception caught: {}", ex);
							//cntFail++;
						}
					}
					long tmWorkerEnd = System.currentTimeMillis();
					return new Long(tmWorkerEnd - tmWorkerStart);
				}
			};
			Future<Long> submit = executor.submit(worker);
			list.add(submit);
		}
		
		log.info("Waiting workers to complete...");
		int wcnt = 0;
		long sumDur = 0;
		for (Future<Long> future : list) {
			try {
				Long workerDur = future.get();
				sumDur += workerDur;
				log.info("Worker #{}: duration={}, avg={}", wcnt++, workerDur, workerDur*1d/howmany);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		double avgWorkerDur = sumDur*1d / nWorkers;
		double avgIterDur = avgWorkerDur / howmany;
		
		long tmEnd = System.currentTimeMillis();
		long dur = tmEnd - tmStart;
		
		log.info("Shutting down executor...");
		try {
			executor.shutdown();
			executor.awaitTermination(1000, TimeUnit.MILLISECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		//log.info("Iterations Total:   {}", cntTotal);
		//log.info("Iterations Success: {}", cntSuccess);
		//log.info("Iterations Fails:   {}", cntFail);
		log.info("Overall Duration: {}ms", dur);
		log.info("Avg. Worker Dur.: {}ms", avgWorkerDur);
		log.info("Avg. time/iter:   {}ms", avgIterDur);
		log.info("**********  END - test-conc **********");
		return String.format("TEST OK: iterations=%d, workers=%d => overall-dur=%d, avg-worker-dur=%f, avg-iter-dur=%f", howmany, nWorkers, dur, avgWorkerDur, avgIterDur);
	}
	
	@ExceptionHandler
	@ResponseStatus(BAD_REQUEST)
	public String handleException(BadRequestException exception) {
		log.error(format("Returning error response: invalid request (%s) ", exception.getMessage()));
		return exception.getMessage();
	}
}
