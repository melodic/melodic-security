/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client.extractor;

import eu.melodic.security.authorization.client.AuthorizationServiceClient;
import eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class DataExtractorHelper {
    private List<DataExtractor> dataExtractors;
	
	public DataExtractorHelper(AuthorizationServiceClient client) {
		this(client.getProperties());
	}
	
	public DataExtractorHelper(AuthorizationServiceClientProperties properties) {
		if (properties.getData()!=null) {
			String currentClazz = null;
			try {
				List<DataExtractor> dataExtractors = new ArrayList<DataExtractor>();
				String[] extractorClass = properties.getData().getExtractors();
				for (String clazz : extractorClass) {
					currentClazz = clazz;
					dataExtractors.add( (DataExtractor) Class.forName(clazz).newInstance() );
					log.debug("Instantiated data extractor: {}", clazz);
				}
				this.dataExtractors = dataExtractors;
			} catch (Exception e) {
				log.error("Could not instantiate data extractors: Problem instantiating {}:", currentClazz, e);
			}
			log.debug("All data extractors instantiated");
		}
	}
	
	public Map<String,Object> getModelData(Object targetModel) {
        Map<String,Object> result = new HashMap<>();
        for (DataExtractor extractor : dataExtractors) {
			log.debug("Getting value from data extractor: {}", extractor.getClass().getName());
			Map valueMap = extractor.getValueMap(targetModel);
			log.debug("Extracted values: {}", valueMap);
			if (valueMap!=null) {
				result.putAll(valueMap);
			}
			String key = extractor.getKey();
			Object value = extractor.getValue(targetModel);
			log.debug("Extracted value: key={}, value={}", key, value);
            result.put(key, value);
			log.trace("Result after data extractor: {}", result);
        }
        return result;
    }
}
