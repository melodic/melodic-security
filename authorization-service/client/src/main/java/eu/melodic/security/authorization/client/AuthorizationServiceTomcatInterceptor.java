/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client;

import eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class AuthorizationServiceTomcatInterceptor implements HandlerInterceptor {
	
	private static AuthorizationServiceTomcatInterceptor singleton;
	
	public synchronized static AuthorizationServiceTomcatInterceptor getSingleton(AuthorizationServiceClientProperties properties) {
		if (singleton==null) singleton = new AuthorizationServiceTomcatInterceptor( properties );
		return singleton;
	}
	
	public synchronized static AuthorizationServiceTomcatInterceptor getSingleton(AuthorizationServiceClientProperties properties, boolean useConfiguredAccessKey, boolean useRequestAccessKey, boolean configuredAccessKeyOverrides) {
		if (singleton==null) singleton = new AuthorizationServiceTomcatInterceptor( properties, useConfiguredAccessKey, useRequestAccessKey, configuredAccessKeyOverrides );
		return singleton;
	}
	
	// --------------------------------------------------------------------------------------------
	// Constructors
	
	private AuthorizationServiceTomcatInterceptor(AuthorizationServiceClientProperties properties) {
		WebAccessRequestHelper.getSingleton(properties);
	}
	
	private AuthorizationServiceTomcatInterceptor(AuthorizationServiceClientProperties properties, boolean useConfiguredAccessKey, boolean useRequestAccessKey, boolean configuredAccessKeyOverrides) {
		WebAccessRequestHelper.getSingleton(properties, useConfiguredAccessKey, useRequestAccessKey, configuredAccessKeyOverrides);
	}
	
	// --------------------------------------------------------------------------------------------
	// HandlerInterceptor API methods
	
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, java.lang.Object handler) throws java.io.IOException, javax.servlet.ServletException {
		return WebAccessRequestHelper.getSingleton().requestWebAccess(request, response);
	}
	
	public void postHandle(HttpServletRequest request, HttpServletResponse response, java.lang.Object handler, ModelAndView modelAndView) { }
	
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, java.lang.Object handler, java.lang.Exception ex) {
		WebAccessRequestHelper.getSingleton().clearRequestContext(request);
	}
}