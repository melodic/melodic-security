/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client;

import eu.melodic.security.authorization.util.CheckAccessRequest;
import eu.melodic.security.authorization.util.CheckAccessResponse;
import eu.melodic.security.authorization.util.HttpClientUtil;
import eu.melodic.security.authorization.util.json.JsonSerializer;
import eu.melodic.security.authorization.util.json.JsonUnserializer;
import eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class AuthorizationServiceClient {
	
	private AuthorizationServiceClientProperties properties;
	private boolean disabled;
	private RestTemplate restTemplate;
	private String PDP_JSON_ENDPOINT;
	private String PDP_ACCESS_TOKEN;
	private String[] pdpEndpoints;
	//private AuthorizationServiceClientProperties.LOAD_BALANCE_METHOD loadBalanceMethod;
	private String loadBalanceMethod;
	private int currentEndpoint = -1;
	private int MAX_PDP_CONNECT_RETRIES;
	
	public AuthorizationServiceClient(AuthorizationServiceClientProperties properties) {
		// retrieve PDP client configuration
		this.properties = properties;
		disabled = properties.getPdp().isDisabled();
		PDP_JSON_ENDPOINT = properties.getPdp().getEndpoints()[0].trim();
		//PDP_JSON_ENDPOINT = properties.getPdp().getBaseUrl().trim();
		PDP_ACCESS_TOKEN = properties.getPdp().getAccessKey().trim();
		pdpEndpoints = properties.getPdp().getEndpoints();
		loadBalanceMethod = properties.getPdp().getLoadBalanceMethod();
		MAX_PDP_CONNECT_RETRIES = properties.getPdp().getRetryCount();
		
		if (MAX_PDP_CONNECT_RETRIES==0) MAX_PDP_CONNECT_RETRIES = pdpEndpoints.length;
		
		log.info("Authorization Service client settings:");
		log.info("Client Enabled:    {}", ! disabled);
		//log.info("PDP JSON endpoint: {}", PDP_JSON_ENDPOINT);
		log.info("PDP endpoints:     {}", Arrays.toString(pdpEndpoints));
		log.info("PDP Access Token:  {}", ! PDP_ACCESS_TOKEN.isEmpty() ? "present" : "missing");
		log.info("Load-Balancing:    {}", loadBalanceMethod);
		log.info("Retry count:       {}", MAX_PDP_CONNECT_RETRIES);
		
		if (disabled) { log.warn("Authorization Service client is DISABLED"); }
		
		// create restTemplate (backed by a configurable http client)
		try {
			restTemplate = HttpClientUtil.createRestTemplate(properties.getPdp().getHttpClient(), pdpEndpoints[0]);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public AuthorizationServiceClientProperties getProperties() {
		return properties;
	}
	
	public RestTemplate getRestTemplate() {
		return restTemplate;
	}
	
	public void setRestTemplate(RestTemplate newRestTemplate) {
		restTemplate = newRestTemplate;
	}
	
	protected synchronized String _getNextEndpoint(boolean forceNext) {
		if (loadBalanceMethod==null || loadBalanceMethod.trim().isEmpty()) {
			loadBalanceMethod = "ORDER";
		}
		loadBalanceMethod = loadBalanceMethod.trim().toUpperCase();
		
		if (loadBalanceMethod.equals("ROUND_ROBIN")) {
			if (pdpEndpoints!=null && pdpEndpoints.length>0) {
				currentEndpoint = (++currentEndpoint) % pdpEndpoints.length;
			}
		} else
		if (loadBalanceMethod.equals("RANDOM")) {
			if (pdpEndpoints!=null && pdpEndpoints.length>0) {
				currentEndpoint = (int)(Math.random() * pdpEndpoints.length);
			}
		} else
		// ORDER, HASH and default
		{
			if (pdpEndpoints!=null && pdpEndpoints.length>0 && (forceNext || currentEndpoint<0)) {
				currentEndpoint = (++currentEndpoint) % pdpEndpoints.length;
			}
		}
		return getPdpEndpoint();
	}
	
	public String getPdpEndpoint() {
		return currentEndpoint<0 ? PDP_JSON_ENDPOINT : pdpEndpoints[currentEndpoint];
	}
	
	public boolean requestAccess(String resource, String action, String subject) throws AccessDeniedException {
		return requestAccess(resource, action, subject, null);
	}
	
	public boolean requestAccess(String resource, String action, String subject, Map<String,Object> extra) throws AccessDeniedException {
		if (disabled) { log.debug("Authorization Service client is disabled"); return true; }
		
		// Collect access request attributes into a Map
		HashMap<String,Object> attrs = new HashMap<String,Object>();
		if (extra!=null) attrs.putAll(extra);
		
		attrs.put("actionId", action);
		attrs.put("resource-id", resource);
		attrs.put("user.identifier", (subject!=null && !subject.isEmpty()) ? subject : PDP_ACCESS_TOKEN);
		
		// request access from PDP
		boolean permit = requestAccess( attrs );
		if (!permit) throw new AccessDeniedException( String.format("'%s' cannot '%s' resource '%s'", subject, action, resource) );
		
		return permit;
	}
		
	public boolean requestAccess(Map<String,Object> attrs) throws AccessDeniedException {
		if (disabled) { log.debug("Authorization Service client is disabled"); return true; }
		
		// Create request to PDP
		CheckAccessRequest request = new CheckAccessRequest();
		request.setId( java.util.UUID.randomUUID().toString() );
		request.setContent( null );
		request.setTimestamp( System.currentTimeMillis() );
		request.setXacmlRequest( attrs );
		
		// Serialize access request to a json message
		log.trace("Request object to serialize into JSON message: {}", request);
		String jsonRequest = new JsonSerializer().serializeObject(request);
		log.trace("JSON message to submit: {}", jsonRequest);
		
		// Contact PDP
		CheckAccessResponse response = null;
		String jsonResponse = null;
		boolean forceNext = false;
		boolean repeat;
		int retryCount = 0;
		do {
			repeat = false;
			try {
				String endpoint = _getNextEndpoint(forceNext);
				log.debug("Access Request to PDP: {} - {}", endpoint, jsonRequest);
				//response = restTemplate.postForObject( endpoint, request, CheckAccessResponse.class );
				jsonResponse = restTemplate.postForObject( endpoint, jsonRequest, String.class );
				
				// Unserialize access response from json message
				log.debug("JSON message received from PDP: {}", jsonResponse);
				response = (CheckAccessResponse) new JsonUnserializer().unserializeObject(jsonResponse);
				//log.trace("JSON message unserialized to a response object: {}", response);
				log.debug("Access Response from PDP: {}", response);
				
			} catch (Throwable th) {
				log.warn("Access Request to PDP failed: ", th);
				if (loadBalanceMethod.equals("ORDER")) {
					retryCount++;
					if (retryCount<MAX_PDP_CONNECT_RETRIES) {
						log.warn("Retrying #{}...", retryCount);
						repeat = true;
						forceNext = true;
					} else {
						log.error("Retry count exceeded");
						throw th;
					}
				} else {
					log.error("No load balance method. Failing immediately");
					throw th;
				}
			}
		} while (repeat);
		
		String decision = response.getContent();
		boolean permit = ( decision!=null && decision.equalsIgnoreCase("PERMIT") );
		
		return permit;
	}
}
