/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client;

import eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

//import org.aspectj.lang.annotation.Before;

@Aspect
@Component
@ComponentScan(basePackages = {"eu.melodic.security.authorization"})
@Slf4j
public class WebAccessRequestAspect implements ApplicationContextAware {
	
	private AuthorizationServiceClientProperties properties;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.properties = applicationContext.getBean(AuthorizationServiceClientProperties.class);
		log.debug("Got authorization client configuration bean");
		
		WebAccessRequestHelper.getSingleton( properties );
		log.debug("WebAccessRequestHelper initialized");
	}
	
	@Around("@annotation(eu.melodic.security.authorization.client.AuthorizationRequired) && @annotation(org.springframework.web.bind.annotation.RequestMapping)")
	public Object authorizeWebAccessRequest(final ProceedingJoinPoint joinPoint) throws Throwable {
		log.debug("Checking web request: {}", joinPoint);
		
		// Get HTTP request from context
		HttpServletRequest request = null;
		if (RequestContextHolder.currentRequestAttributes()!=null) {
			request = ((ServletRequestAttributes) RequestContextHolder
					.currentRequestAttributes())
					.getRequest();
		}
		log.debug("Request: {}", request);
		if (request==null) {
			log.debug("It's a local call. Permit access");
			return joinPoint.proceed();
		}
		
		// Check request if authorized
		boolean permit = false;
		permit = WebAccessRequestHelper.getSingleton().checkWebAccessFromRequest(request);
		if (!permit) throw new AccessDeniedException("Not authorized to access resource: "+request.getRequestURI());
		
		// Allow annotated method (web endpoint) to execute
		log.info("Web request authorized: {}", request.getRequestURI());
		Object retVal = joinPoint.proceed();
		
		// Clear request context
		WebAccessRequestHelper.getSingleton().clearRequestContext(request);
		
		return retVal;
	}
}
