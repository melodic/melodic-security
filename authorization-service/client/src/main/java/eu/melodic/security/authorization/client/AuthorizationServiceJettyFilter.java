/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client;

import eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
public class AuthorizationServiceJettyFilter implements Filter, ApplicationContextAware {
	
	private AuthorizationServiceClientProperties properties;
	private ServletContext _context;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.properties = applicationContext.getBean(AuthorizationServiceClientProperties.class);
		log.debug("Got authorization client configuration bean");
	}
	
	public void init(FilterConfig filterConfig) {
		log.debug("Initializing AuthorizationServiceJettyFilter...");
		_context = filterConfig.getServletContext();
		
		WebAccessRequestHelper.getSingleton( properties );
		log.debug("WebAccessRequestHelper initialized");
	}
	
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		log.debug("Filtering request: {}", request);
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		
		// Check request if authorized
		boolean permit = WebAccessRequestHelper.getSingleton().requestWebAccess(httpServletRequest, httpServletResponse);
		if (permit) {
			log.debug("Request authorization successful. Calling next filter in the chain...");
			chain.doFilter(request, response);
		} else {
			log.debug("Request authorization failure. Processing stopped");
		}
		log.trace("Filtering request: done");
	}
	
	public void destroy() {
		log.debug("Destroying AuthorizationServiceJettyFilter...");
	}
}