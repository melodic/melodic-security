/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client;

import eu.melodic.security.authorization.context.ContextClientUtil;
import eu.melodic.security.authorization.context.ContextRepositoryClient;
import eu.melodic.security.authorization.context.extractor.HttpRequestContextExtractor;
import eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class WebAccessRequestHelper {

	private static final String JWT_TOKEN_PREFIX = "Bearer ";
	private static final String JWT_HEADER_STRING = "Authorization";
	private static String jwtSecret;

	private static WebAccessRequestHelper singleton;
	
	public synchronized static WebAccessRequestHelper getSingleton(AuthorizationServiceClientProperties properties) {
		if (singleton==null) singleton = new WebAccessRequestHelper( properties );
		return singleton;
	}
	
	public synchronized static WebAccessRequestHelper getSingleton(AuthorizationServiceClientProperties properties, boolean useConfiguredAccessKey, boolean useRequestAccessKey, boolean configuredAccessKeyOverrides) {
		if (singleton==null) singleton = new WebAccessRequestHelper( properties, useConfiguredAccessKey, useRequestAccessKey, configuredAccessKeyOverrides );
		return singleton;
	}
	
	public synchronized static WebAccessRequestHelper getSingleton() {
		return singleton;
	}
	
	// --------------------------------------------------------------------------------------------
	// Fields, constants and constructors
	
	public final static String REQUEST_ID_ATTRIBUTE_NAME = "eu.melodic.security.authorization.context.WebAccessRequestHelper.REQUEST-ID";
	
	private AuthorizationServiceClient pdpClient;
	private ContextRepositoryClient contextReposClient;
	private HttpRequestContextExtractor extractor;
	private boolean useConfiguredAccessKey = true;
	private boolean useRequestAccessKey = true;
	private boolean configuredAccessKeyOverrides = false;
	private String accessKey = null;
	
	private WebAccessRequestHelper(AuthorizationServiceClientProperties properties) {
		// initialize PDP client
		this.pdpClient = new AuthorizationServiceClient(properties);
		
		// initialize client-side request context extraction, if configured
		this.extractor = (HttpRequestContextExtractor) ContextClientUtil.createExtractor( properties.getContext() );
		this.contextReposClient = (extractor!=null) ? extractor.getContextRepositoryClient() : null;

		// initialize JWT secret
		setJwtSecret(properties.getJwtSecret());
	}
	
	private WebAccessRequestHelper(AuthorizationServiceClientProperties properties, boolean useConfiguredAccessKey, boolean useRequestAccessKey, boolean configuredAccessKeyOverrides) {
		this(properties);
		this.useConfiguredAccessKey = useConfiguredAccessKey;
		this.useRequestAccessKey = useRequestAccessKey;
		this.configuredAccessKeyOverrides = configuredAccessKeyOverrides;
		this.accessKey = properties.getPdp().getAccessKey();
	}
	
	// --------------------------------------------------------------------------------------------
	// Getter methods
	
	public AuthorizationServiceClient getAuthorizationServiceClient() {
		return this.pdpClient;
	}
	
	public HttpRequestContextExtractor getHttpRequestContextExtractor() {
		return this.extractor;
	}
	
	public ContextRepositoryClient getContextRepositoryClient() {
		return this.contextReposClient;
	}
	
	// --------------------------------------------------------------------------------------------
	// Web Access Request methods
	
	public boolean requestWebAccess(HttpServletRequest request, HttpServletResponse response) throws java.io.IOException, javax.servlet.ServletException {
		log.debug("Request:    {} {}", request.getMethod(), request.getRequestURL());
		log.debug("Access Key: {}", request.getHeader("melodic-access-key"));
		
		boolean permit = false;
		String mesg = null;
		try {
			permit = checkWebAccessFromRequest(request);
			if (!permit) mesg = "Not authorized access to resource: "+request.getRequestURI();
		} catch (Exception ex) {
			mesg = "An error occurred while evaluating access request";
		}
		
		// Handle response in case of authorization failure (deny/error etc)
		if (!permit) {
			log.warn("Access Denied to web request: {} - {}", request.getRequestURI(), mesg);
			response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			new java.io.PrintStream(response.getOutputStream()).println(mesg);
		} else {
			log.info("Web Request authorized: {}", request.getRequestURI());
		}
		return permit;
	}
	
	// --------------------------------------------------------------------------------------------
	// Package-accessible methods
	
	boolean checkWebAccessFromRequest(HttpServletRequest request) {
		// PEP-level context extraction to occur here
		long requestId = -1;
		if (extractor!=null) {
			log.debug("Sending request context to context server");
			requestId = extractor.extractContext(request);
			if (requestId>=0) request.setAttribute(REQUEST_ID_ATTRIBUTE_NAME, requestId);
			log.debug("Request context stored");
			log.debug("Context server assigned Request-Id: {}", requestId);
		}
		
		// Collect mandatory info needed by PDP client
		String subject = null;
		if (useRequestAccessKey) {
			subject = request.getHeader("melodic-access-key");
			if (subject==null) { subject = request.getParameter("melodic-access-key"); }		//XXX: POSSIBLY-A-BUG: In case of HTTP POST getParameter() might interfere with reading request body with getInputStream() or getReader()
			if (subject==null) subject = getAccessKeyFromQueryString( request.getQueryString() );
		}
		if (useConfiguredAccessKey && (subject==null || subject!=null && configuredAccessKeyOverrides)) {
			subject = accessKey;
		}
		if (subject==null) subject = "";
		
		String action = request.getMethod();
		String resource = request.getRequestURI();
		log.debug("PDP client will be invoked with: subject={}, action={}, resource={}", subject, action, resource);
		
		// Collect other HTTP request info as extra request attributes
		//HashMap<String,String> extra = new HashMap<String,String>();
		HashMap<String,Object> extra = new HashMap<String,Object>();
		collectRequestAttributes(request, extra);
		
		// Collect JWT token info (if provided) as extra request attributes
		collectJwtAttributes(request, extra);

		// Use PDP client to evaluate access request against policies
		boolean permit = false;
		String mesg = null;
		try {
			if (requestId>=0) {
				extra.put("request-id", Long.toString(requestId));
			}

			log.debug("Extra attributes added: {}", extra);
			log.debug("Calling PDP: {}", pdpClient.getPdpEndpoint());
			permit = pdpClient.requestAccess(resource, action, subject, extra);
			log.debug("PDP decision: {}", permit);
		}
		catch (AccessDeniedException adex) { }
		catch (Exception ex) { log.warn("An error occurred while evaluating web access request: ", ex); throw ex; }
		
		return permit;
	}
	
	void clearRequestContext(HttpServletRequest request) {
		Object o = request.getAttribute(REQUEST_ID_ATTRIBUTE_NAME);
		if (o!=null) {
			long requestId = ((Long)o).longValue();
			if (requestId>=0) {
				log.info("Clearing context for request: {}", requestId);
				this.contextReposClient.deleteContextForRequest(requestId);
			}
		}
	}
	
	// --------------------------------------------------------------------------------------------
	// Non-public methods
	
	protected String getAccessKeyFromQueryString(String qs) {
		log.debug("Query String: {}", qs);
		if (qs!=null) {
			int p = -1;
			if (qs.startsWith("melodic-access-key=")) p = 0;
			else p = qs.indexOf("&melodic-access-key=");
			if (p>-1) {
				p = qs.indexOf("=",p);
				p++;
				if (p<qs.length()) {
					int p2 = qs.indexOf("&", p);
					if (p2>p) return qs.substring(p,p2).trim();
					else return qs.substring(p).trim();
				}
			}
		}
		return null;
	}
	
	protected void collectRequestAttributes(HttpServletRequest request, Map<String,Object> extra) {
		// print request attributes
		log.debug("auth-type:      {}", request.getAuthType());
		log.debug("method:         {}", request.getMethod());
		log.debug("protocol:       {}", request.getProtocol());
		log.debug("scheme:         {}", request.getScheme());
		log.debug("secure:         {}", request.isSecure());
		//log.debug("cookies:        {}", request.getCookies());
		log.debug("parameters:     {}", prepareParameterMap( request.getParameterMap() ));
		log.debug("encoding:       {}", request.getCharacterEncoding());
		log.debug("content-type:   {}", request.getContentType());
		log.debug("remote-address: {}", request.getRemoteAddr());
		log.debug("remote-host:    {}", request.getRemoteHost());
		log.debug("remote-port:    {}", request.getRemotePort());
		log.debug("session-valid:  {}", request.isRequestedSessionIdValid());
		
		// Add collected info to 'extra' map
		extra.put("auth-type", request.getAuthType());
		extra.put("method", request.getMethod());
		extra.put("protocol", request.getProtocol());
		extra.put("scheme", request.getScheme());
		extra.put("secure", request.isSecure());
		//extra.put("cookies", request.getCookies());		//XXX:BUG: This line seems to cause self-recursion when serializing cookies field
		extra.put("parameters", new HashMap( request.getParameterMap() ));
		extra.put("encoding", request.getCharacterEncoding());
		extra.put("content-type", request.getContentType());
		extra.put("remote-address", request.getRemoteAddr());
		extra.put("remote-host", request.getRemoteHost());
		extra.put("remote-port", request.getRemotePort());
		extra.put("session-valid", request.isRequestedSessionIdValid());
	}

	public void setJwtSecret(String secret) {
		if (StringUtils.isEmpty(secret)) secret = null;
		jwtSecret = secret;
	}

	protected void collectJwtAttributes(HttpServletRequest request, Map<String,Object> extra) {
		// JWT secret has not been set
		if (StringUtils.isEmpty(jwtSecret)) {
			log.debug("collectJwtAttributes(): JWT secret has not been set");
			return;
		}

		// get JWT token from Header
		String token = request.getHeader(JWT_HEADER_STRING);
		if (token == null || !token.startsWith(JWT_TOKEN_PREFIX)) {
			log.debug("collectJwtAttributes(): No Authorization header found or it is not JWT: {}", token);
			return;
		}

		// get claims from JWT token
		Claims claims = Jwts.parser()
				.setSigningKey(jwtSecret.getBytes())
				.parseClaimsJws(token.replace(JWT_TOKEN_PREFIX, ""))
				.getBody();
		log.debug("jwt-audience:   {}", claims.getAudience());
		log.debug("jwt-expiration: {}", claims.getExpiration());
		log.debug("jwt-id:         {}", claims.getId());
		log.debug("jwt-issuedAt:   {}", claims.getIssuedAt());
		log.debug("jwt-issuer:     {}", claims.getIssuer());
		log.debug("jwt-notBefore:  {}", claims.getNotBefore());
		log.debug("jwt-subject:    {}", claims.getSubject());

		// add JWT claims to 'extra' map
		extra.put("jwt-audience", claims.getAudience());
		extra.put("jwt-expiration", claims.getExpiration());
		extra.put("jwt-id", claims.getId());
		extra.put("jwt-issuedAt", claims.getIssuedAt());
		extra.put("jwt-issuer", claims.getIssuer());
		extra.put("jwt-notBefore", claims.getNotBefore());
		extra.put("jwt-subject", claims.getSubject());
	}

	protected Map<String,String> prepareParameterMap(Map parameters) {
		Map<String,String> map = new HashMap<String,String>();
		for (Object name : parameters.keySet()) {
			Object value = parameters.get(name);
			String nameStr  = name!=null ? name.toString() : null;
			String valueStr = null;
			if (value!=null) {
				valueStr = value.getClass().isArray()
						? Arrays.toString( (Object[])value )
						: value.toString();
			}
			map.put(nameStr, valueStr);
		}
		return map;
	}
}