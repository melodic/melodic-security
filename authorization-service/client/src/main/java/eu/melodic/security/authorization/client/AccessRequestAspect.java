/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client;

import eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

@Aspect
@Component
@ComponentScan(basePackages = {"eu.melodic.security.authorization"})
@Slf4j
public class AccessRequestAspect implements ApplicationContextAware {
	
	private AuthorizationServiceClient pdpClient;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		log.debug("Setting application context");
		AuthorizationServiceClientProperties properties = applicationContext.getBean(AuthorizationServiceClientProperties.class);
		if (pdpClient==null) pdpClient = WebAccessRequestHelper.getSingleton( properties ).getAuthorizationServiceClient();
	}
	
	@Around("@annotation(eu.melodic.security.authorization.client.AuthorizationRequired) && !@annotation(org.springframework.web.bind.annotation.RequestMapping)")
	public Object authorizeAccessRequest(ProceedingJoinPoint joinPoint) throws Throwable {
		log.debug("Checking call: {}", joinPoint);
		
		// Collect access request attributes
		String resource = joinPoint.getThis().toString();			// Use the FQCN or Object Instance being accessed
		String[] sigPart = joinPoint.getSignature().toLongString().split(" ");
		String action = sigPart[sigPart.length-1];					// Use the method signature from joinPoint
		String subject = null;										// null means use PDP Access Key (if present in authorization.properties)
		
		// Call PDP to evaluate access request against policies
		try {
			log.debug("Calling PDP: {}", pdpClient.getPdpEndpoint());
			boolean permit = pdpClient.requestAccess(resource, action, subject);
			log.debug("PDP decision: {}", permit);
			if (!permit) throw new AccessDeniedException("Access is denied at " + joinPoint.toString());
		}
		catch (AccessDeniedException adex) { throw adex; }
		catch (Exception ex) { throw new AccessDeniedException("An error occurred while evaluating access request", ex); }
		
		// Access is Permitted. Allow the invocation of the annotated method (i.e. call PROCEED)
		log.info("Method call authorized: {}", action);
		return joinPoint.proceed();
	}
}
