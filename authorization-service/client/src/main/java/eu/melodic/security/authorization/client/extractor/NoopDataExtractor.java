/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client.extractor;

import java.util.Map;

public class NoopDataExtractor implements DataExtractor<Object,String> {

    public String getKey() { return "NoopDataExtractor"; }

    public String getValue(Object model) { return "eu.melodic.security.authorization.client.extractor.NoopDataExtractor: model="+model; }

    public Map<String,String> getValueMap(Object model) { return null; }
}
