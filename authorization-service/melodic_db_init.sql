--
-- Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
--
-- This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
-- If a copy of the MPL was not distributed with this file, You can obtain one at
-- https://www.mozilla.org/en-US/MPL/2.0/
--

-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.5-10.1.10-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             8.3.0.4694
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for melodic_db
DROP DATABASE IF EXISTS `melodic_db`;
CREATE DATABASE IF NOT EXISTS `melodic_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `melodic_db`;


-- Dumping structure for table melodic_db.access_keys
DROP TABLE IF EXISTS `access_keys`;
CREATE TABLE IF NOT EXISTS `access_keys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity` varchar(100) NOT NULL,
  `key` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQUE_ENTITY` (`entity`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- Dumping data for table melodic_db.access_keys: ~1 rows (approximately)
/*!40000 ALTER TABLE `access_keys` DISABLE KEYS */;
INSERT INTO `access_keys` (`id`, `entity`, `key`) VALUES
	(1, 'test', '7235687126587231675321756752657236156321765723');
/*!40000 ALTER TABLE `access_keys` ENABLE KEYS */;


-- Dumping structure for table melodic_db.context_elements
DROP TABLE IF EXISTS `context_elements`;
CREATE TABLE IF NOT EXISTS `context_elements` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(1000) NOT NULL,
  `value` mediumtext,
  `scope` varchar(1000) NOT NULL,
  `timestamp` bigint(20) NOT NULL,
  `request_id` bigint(20) DEFAULT '-1',
  `del_flag` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `Name Index` (`name`(255)),
  KEY `Request_id index` (`request_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


-- Dumping structure for table melodic_db.requests
DROP TABLE IF EXISTS `requests`;
CREATE TABLE IF NOT EXISTS `requests` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
