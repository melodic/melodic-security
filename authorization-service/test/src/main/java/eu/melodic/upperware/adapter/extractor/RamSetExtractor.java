/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.upperware.adapter.extractor;

import camel.deployment.DeploymentInstanceModel;
import eu.melodic.security.authorization.client.extractor.DataExtractor;
import io.github.cloudiator.rest.model.NodeCandidate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class RamSetExtractor extends NodeCandidateSupport implements DataExtractor<DeploymentInstanceModel,Set<Long>> {
    @Override
    public String getKey() {
        return "set-of-ram";
    }

    @Override
    public Set<Long> getValue(DeploymentInstanceModel deploymentModel) {
        Map<String, NodeCandidate> nodeCandidateForDeployment = getNodeCandidateForDeployment(deploymentModel);
        return nodeCandidateForDeployment
                .values()
                .stream()
                .map(value -> value.getHardware().getRam())
                .collect(Collectors.toSet());
    }

    @Override
    public Map<String,Set<Long>> getValueMap(DeploymentInstanceModel deploymentModel) {
        return null;
    }
}
