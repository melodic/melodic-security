/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.upperware.adapter.extractor;

import io.github.cloudiator.rest.model.GeoLocation;
import io.github.cloudiator.rest.model.Location;
import io.github.cloudiator.rest.model.NodeCandidate;
import org.springframework.stereotype.Service;

@Service
public class PerVmCountryExtractor extends PerVmAbstractExtractor<String> {
    @Override
    public String getKey() {
        return "per-vm-countries";
    }

    @Override
	protected String extractInfo(NodeCandidate nodeCandidate) {
		String vmCountry = null;
		Location vmLoc = nodeCandidate.getHardware().getLocation();
		if (vmLoc==null) vmLoc = nodeCandidate.getLocation();
		if (vmLoc!=null) {
			GeoLocation geoLoc = vmLoc.getGeoLocation();
			if (geoLoc!=null) {
				vmCountry = geoLoc.getCountry();
			}
		}
		return com.google.common.base.Strings.nullToEmpty(vmCountry);
    }
}
