/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.upperware.adapter.extractor;

import camel.deployment.DeploymentInstanceModel;
import eu.melodic.security.authorization.client.extractor.DataExtractor;
import io.github.cloudiator.rest.model.NodeCandidate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.stream.Collectors;

@Service
public abstract class PerVmAbstractExtractor<T> extends NodeCandidateSupport implements DataExtractor<DeploymentInstanceModel,Map<String,T>> {
    @Override
    public Map<String,T> getValue(DeploymentInstanceModel deploymentModel) {
        Map<String, NodeCandidate> nodeCandidateForDeployment = getNodeCandidateForDeployment(deploymentModel);
        return nodeCandidateForDeployment
                .entrySet()
                .stream()
                .collect(Collectors.toMap(
					Map.Entry::getKey,
					entry -> extractInfo(entry.getValue())
				));
    }
	
	protected abstract T extractInfo(NodeCandidate nodeCandidate);

    @Override
    public Map<String,Map<String,T>> getValueMap(DeploymentInstanceModel deploymentModel) {
		return null;
    }
}
