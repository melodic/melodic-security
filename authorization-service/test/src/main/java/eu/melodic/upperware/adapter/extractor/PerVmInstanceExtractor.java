/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.upperware.adapter.extractor;

import camel.deployment.DeploymentInstanceModel;
import camel.deployment.VMInstance;
import eu.melodic.security.authorization.client.extractor.DataExtractor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class PerVmInstanceExtractor extends NodeCandidateSupport implements DataExtractor<DeploymentInstanceModel,Map<String,Integer>> {
    @Override
    public String getKey() {
        return "per-vm-type-instances";
    }

    @Override
    public Map<String,Integer> getValue(DeploymentInstanceModel deploymentModel) {
        Map<String, Integer> vmInstanceCount = new HashMap<>();
		for (VMInstance vmi : deploymentModel.getVmInstances()) {
			String vmType = vmi.getType().getName();
			vmInstanceCount.merge(vmType, 1, Integer::sum);
		}
        return vmInstanceCount;
    }
	
    @Override
    public Map<String,Map<String,Integer>> getValueMap(DeploymentInstanceModel deploymentModel) {
		return null;
    }
}
