/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.upperware.adapter.extractor;

//import camel.core.NamedElement;
import camel.deployment.ComponentInstance;
import camel.deployment.DeploymentInstanceModel;
import camel.type.StringValue;
import eu.melodic.security.authorization.client.extractor.DataExtractor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ComponentExtractor extends NodeCandidateSupport implements DataExtractor<DeploymentInstanceModel,Set<String>> {
    @Override
    public String getKey() {
        return "all-vm-components";
    }

    @Override
    public Set<String> getValue(DeploymentInstanceModel deploymentModel) {
        Set<String> results = deploymentModel.getSoftwareComponentInstances()
                .stream()
                .map(ComponentInstance::getType)
                .filter(Objects::nonNull)
                .map(ci -> {
                    if (ci==null) return null;
                    String name = ci.getName();
                    if (StringUtils.isBlank(name)) return null;

                    Optional<StringValue> optVer = ci.getAttributes().stream()
                            .filter(at -> "Version".equalsIgnoreCase(at.getName()))
                            .map(at -> at.getValue())
                            .filter(v -> v instanceof camel.type.StringValue)
                            .map(v -> (StringValue)v)
                            .findFirst();
                    String version = (optVer.isPresent()) ? optVer.get().getValue() : "";

//                    Map<String,String> info = new HashMap<String,String>();
//                    info.put("component", name);
//                    info.put("version", version);
//                    return info;

                    return (StringUtils.isNotBlank(version)) ? name+" #"+version : name;
                })
                //.map(NamedElement::getName)
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.toSet());

        // add a 'null' value to avoid getting an INDETERMINATE response from authorization server
        if (results.size()==0) {
            results = new java.util.HashSet<String>();
            results.add(null);
        }
        return results;
    }

    @Override
    public Map<String,Set<String>> getValueMap(DeploymentInstanceModel deploymentModel) {
        return null;
    }
}
