/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.upperware.adapter.extractor;

import camel.deployment.DeploymentInstanceModel;
import eu.melodic.security.authorization.client.extractor.DataExtractor;
import io.github.cloudiator.rest.model.NodeCandidate;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class StorageExtractor extends NodeCandidateSupport implements DataExtractor<DeploymentInstanceModel,Double> {

    @Override
    public String getKey() {
        return "total-storage";
    }

    @Override
    public Double getValue(DeploymentInstanceModel deploymentModel) {
        Map<String, NodeCandidate> nodeCandidateForDeployment = getNodeCandidateForDeployment(deploymentModel);
        return nodeCandidateForDeployment
                .values()
                .stream()
                .mapToDouble(value -> value.getHardware().getDisk())
                .sum();
    }

    @Override
    public Map<String,Double> getValueMap(DeploymentInstanceModel deploymentModel) {
        return null;
    }
}
