/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.upperware.adapter.extractor;

import camel.deployment.DeploymentInstanceModel;
import eu.melodic.security.authorization.client.extractor.DataExtractor;
import io.github.cloudiator.rest.model.GeoLocation;
import io.github.cloudiator.rest.model.Location;
import io.github.cloudiator.rest.model.NodeCandidate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class CountryExtractor extends NodeCandidateSupport implements DataExtractor<DeploymentInstanceModel,Set<String>> {
    @Override
    public String getKey() {
        return "all-vm-countries";
    }

    @Override
    public Set<String> getValue(DeploymentInstanceModel deploymentModel) {
        Map<String, NodeCandidate> nodeCandidateForDeployment = getNodeCandidateForDeployment(deploymentModel);
        return nodeCandidateForDeployment
                .values()
                .stream()
                .map(value -> {
					Location vmLoc = value.getHardware().getLocation();
					if (vmLoc==null) vmLoc = value.getLocation();
					if (vmLoc==null) return null;
					GeoLocation geoLoc = vmLoc.getGeoLocation();
					if (geoLoc==null) return null;
					return geoLoc.getCountry();
				})
                .collect(Collectors.toSet());
    }

    @Override
    public Map<String,Set<String>> getValueMap(DeploymentInstanceModel deploymentModel) {
        return null;
    }
}
