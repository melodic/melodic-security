/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.upperware.adapter.extractor;

import camel.deployment.DeploymentInstanceModel;
import camel.deployment.VMInstance;
import eu.melodic.security.authorization.client.extractor.DataExtractor;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class InstanceSetExtractor extends NodeCandidateSupport implements DataExtractor<DeploymentInstanceModel,Set<Integer>> {

    @Override
    public String getKey() {
        return "set-of-instances";
    }

    @Override
    public Set<Integer> getValue(DeploymentInstanceModel deploymentModel) {
        return deploymentModel.getVmInstances()
                .stream()
				.collect(Collectors.groupingBy(VMInstance::getType, Collectors.counting()))
				.values()
				.stream()
				.map(c -> (int)c.longValue())
                .collect(Collectors.toSet());
    }

    @Override
    public Map<String,Set<Integer>> getValueMap(DeploymentInstanceModel deploymentModel) {
        return null;
    }
}
