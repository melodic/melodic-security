/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.upperware.adapter.extractor;

import camel.deployment.DeploymentInstanceModel;
import eu.melodic.security.authorization.client.extractor.DataExtractor;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class InstanceExtractor extends NodeCandidateSupport implements DataExtractor<DeploymentInstanceModel,Integer> {

    @Override
    public String getKey() {
        return "total-number-of-instances";
    }

    @Override
    public Integer getValue(DeploymentInstanceModel deploymentModel) {
		return deploymentModel.getVmInstances().size();
    }

    @Override
    public Map<String,Integer> getValueMap(DeploymentInstanceModel deploymentModel) {
        return null;
    }
}
