/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.test;

import java.util.stream.Collectors;

@lombok.extern.slf4j.Slf4j
public class PreauthorizationTest {

	public static void main(String[] args) throws Exception {	
		String command = args[0].trim().toLowerCase();
		String tc_id = args[1];
		if ("create".equals(command)) createCamelModel(tc_id);
		else if ("test".equals(command)) testPreauthorizeCamelModel(tc_id);
		else System.err.println("Unknown command: "+command);
	}
		
	public static void createCamelModel(String tc_id) throws Exception {
		// Create Deployment Type Model
		camel.deployment.VM vmt_A = camel.deployment.DeploymentFactory.eINSTANCE.createVM();
		camel.deployment.VM vmt_B = camel.deployment.DeploymentFactory.eINSTANCE.createVM();
		vmt_A.setName("TC-AC-"+tc_id+"-VMT-A");
		vmt_B.setName("TC-AC-"+tc_id+"-VMT-B");
		
		camel.deployment.DeploymentTypeModel deploymentTypeModel = null;
		deploymentTypeModel = camel.deployment.DeploymentFactory.eINSTANCE.createDeploymentTypeModel();
		deploymentTypeModel.setName("TC-AC-"+tc_id+"-DTM");
		deploymentTypeModel.getVms().add(vmt_A);
		deploymentTypeModel.getVms().add(vmt_B);
		
		// Create Location Model
		camel.location.LocationModel locationModel = camel.location.LocationFactory.eINSTANCE.createLocationModel();
		locationModel.setName("TC-AC-"+tc_id+"-LM");
		camel.location.GeographicalRegion EU = camel.location.LocationFactory.eINSTANCE.createGeographicalRegion();
		camel.location.GeographicalRegion DE = camel.location.LocationFactory.eINSTANCE.createGeographicalRegion();
		camel.location.GeographicalRegion GR = camel.location.LocationFactory.eINSTANCE.createGeographicalRegion();
		EU.setId("TC-AC-"+tc_id+"-EU"); EU.setName("TC-AC-"+tc_id+"-EU");
		DE.setId("TC-AC-"+tc_id+"-DE"); DE.setName("TC-AC-"+tc_id+"-DE"); DE.getParentRegions().add(EU);
		GR.setId("TC-AC-"+tc_id+"-GR"); GR.setName("TC-AC-"+tc_id+"-GR"); GR.getParentRegions().add(EU);
		locationModel.getRegions().add(EU);
		locationModel.getRegions().add(DE);
		locationModel.getRegions().add(GR);
		
		
		// prepare node candidate specifications (in JSON)
		/*io.github.cloudiator.rest.model.GeoLocation geoEU = new io.github.cloudiator.rest.model.GeoLocation();
		io.github.cloudiator.rest.model.GeoLocation geoDE = new io.github.cloudiator.rest.model.GeoLocation();
		io.github.cloudiator.rest.model.GeoLocation geoGR = new io.github.cloudiator.rest.model.GeoLocation();
		geoEU.setCountry("EU");
		geoDE.setCountry("DE");
		geoGR.setCountry("GR");
		
		io.github.cloudiator.rest.model.Location locEU = new io.github.cloudiator.rest.model.Location();
		io.github.cloudiator.rest.model.Location locDE = new io.github.cloudiator.rest.model.Location();
		io.github.cloudiator.rest.model.Location locGR = new io.github.cloudiator.rest.model.Location();
		locEU.setName("Europe");  locEU.setGeoLocation(geoEU);
		locDE.setName("Germany"); locDE.setGeoLocation(geoDE); locDE.setParent(locEU);
		locGR.setName("Greece");  locGR.setGeoLocation(geoGR); locGR.setParent(locEU);
		
		io.github.cloudiator.rest.model.Hardware hw_1 = new io.github.cloudiator.rest.model.Hardware();
		io.github.cloudiator.rest.model.Hardware hw_2 = new io.github.cloudiator.rest.model.Hardware();
		io.github.cloudiator.rest.model.Hardware hw_3 = new io.github.cloudiator.rest.model.Hardware();
		io.github.cloudiator.rest.model.Hardware hw_4 = new io.github.cloudiator.rest.model.Hardware();
		hw_1.setCores(4);  hw_1.setRam(16L);  hw_1.setDisk(512.0);  hw_1.setLocation(locEU);
		hw_2.setCores(5);  hw_2.setRam(17L);  hw_2.setDisk(513.0);  hw_2.setLocation(locDE);
		hw_3.setCores(6);  hw_3.setRam(18L);  hw_3.setDisk(514.0);  hw_3.setLocation(locGR);
		hw_4.setCores(7);  hw_4.setRam(19L);  hw_4.setDisk(515.0);  //hw_4.setLocation(locGR);
		io.github.cloudiator.rest.model.NodeCandidate nc_1 = new io.github.cloudiator.rest.model.NodeCandidate();
		io.github.cloudiator.rest.model.NodeCandidate nc_2 = new io.github.cloudiator.rest.model.NodeCandidate();
		io.github.cloudiator.rest.model.NodeCandidate nc_3 = new io.github.cloudiator.rest.model.NodeCandidate();
		io.github.cloudiator.rest.model.NodeCandidate nc_4 = new io.github.cloudiator.rest.model.NodeCandidate();
		nc_1.setId("nc_1");  nc_1.setHardware(hw_1);
		nc_2.setId("nc_2");  nc_2.setHardware(hw_2);
		nc_3.setId("nc_3");  nc_3.setHardware(hw_3);
		nc_4.setId("nc_4");  nc_4.setHardware(hw_4);
		
		com.google.gson.Gson gson = new com.google.gson.Gson();
		String ncJson_1 = gson.toJson(nc_1);
		String ncJson_2 = gson.toJson(nc_2);
		String ncJson_3 = gson.toJson(nc_3);
		String ncJson_4 = gson.toJson(nc_4);
		
		System.out.println(">>>   "+ncJson_1);
		System.out.println(">>>   "+ncJson_2);
		System.out.println(">>>   "+ncJson_3);
		System.out.println(">>>   "+ncJson_4);*/
		
		String ncJson_1 = "{\"id\":\"nc_1\",\"hardware\":{\"cores\":4,\"ram\":16,\"disk\":512.0,\"location\":{\"name\":\"Europe\",\"geoLocation\":{\"country\":\"EU\"}}}}";
		String ncJson_2 = "{\"id\":\"nc_2\",\"hardware\":{\"cores\":5,\"ram\":17,\"disk\":513.0,\"location\":{\"name\":\"Germany\",\"geoLocation\":{\"country\":\"DE\"},\"parent\":{\"name\":\"Europe\",\"geoLocation\":{\"country\":\"EU\"}}}}}";
		String ncJson_3 = "{\"id\":\"nc_3\",\"hardware\":{\"cores\":6,\"ram\":18,\"disk\":514.0,\"location\":{\"name\":\"Greece\",\"geoLocation\":{\"country\":\"GR\"},\"parent\":{\"name\":\"Europe\",\"geoLocation\":{\"country\":\"EU\"}}}}}";
		String ncJson_4 = "{\"id\":\"nc_4\",\"hardware\":{\"cores\":7,\"ram\":19,\"disk\":515.0}}";
		
		
		// create a 'targetModel'
		camel.type.StringValue ncValue_1 = camel.type.TypeFactory.eINSTANCE.createStringValue();
		camel.type.StringValue ncValue_2 = camel.type.TypeFactory.eINSTANCE.createStringValue();
		camel.type.StringValue ncValue_3 = camel.type.TypeFactory.eINSTANCE.createStringValue();
		camel.type.StringValue ncValue_4 = camel.type.TypeFactory.eINSTANCE.createStringValue();
		ncValue_1.setValue(ncJson_1);
		ncValue_2.setValue(ncJson_2);
		ncValue_3.setValue(ncJson_3);
		ncValue_4.setValue(ncJson_4);
		
		camel.core.Attribute hwAttr_1 = camel.core.CoreFactory.eINSTANCE.createAttribute();
		camel.core.Attribute hwAttr_2 = camel.core.CoreFactory.eINSTANCE.createAttribute();
		camel.core.Attribute hwAttr_3 = camel.core.CoreFactory.eINSTANCE.createAttribute();
		camel.core.Attribute hwAttr_4 = camel.core.CoreFactory.eINSTANCE.createAttribute();
		hwAttr_1.setName("nodeCandidate"); hwAttr_1.setValue(ncValue_1);
		hwAttr_2.setName("nodeCandidate"); hwAttr_2.setValue(ncValue_2);
		hwAttr_3.setName("nodeCandidate"); hwAttr_3.setValue(ncValue_3);
		hwAttr_4.setName("nodeCandidate"); hwAttr_4.setValue(ncValue_4);
		
		camel.deployment.VMInstance vmi_1 = camel.deployment.DeploymentFactory.eINSTANCE.createVMInstance();
		camel.deployment.VMInstance vmi_2 = camel.deployment.DeploymentFactory.eINSTANCE.createVMInstance();
		camel.deployment.VMInstance vmi_3 = camel.deployment.DeploymentFactory.eINSTANCE.createVMInstance();
		camel.deployment.VMInstance vmi_4 = camel.deployment.DeploymentFactory.eINSTANCE.createVMInstance();
		vmi_1.setName("TC-AC-"+tc_id+"-VMI-A-1");  vmi_1.getAttributes().add( hwAttr_1 );	vmi_1.setLocation(EU);	vmi_1.setType(vmt_A);
		vmi_2.setName("TC-AC-"+tc_id+"-VMI-A-2");  vmi_2.getAttributes().add( hwAttr_2 );	vmi_2.setLocation(DE);	vmi_2.setType(vmt_A);
		vmi_3.setName("TC-AC-"+tc_id+"-VMI-B-1");  vmi_3.getAttributes().add( hwAttr_3 );	vmi_3.setLocation(GR);	vmi_3.setType(vmt_B);
		vmi_4.setName("TC-AC-"+tc_id+"-VMI-B-2");  vmi_4.getAttributes().add( hwAttr_4 );	vmi_4.setLocation(DE);	vmi_4.setType(vmt_B);
		
		camel.deployment.DeploymentInstanceModel targetModel = null;
		targetModel = camel.deployment.DeploymentFactory.eINSTANCE.createDeploymentInstanceModel();
		targetModel.setName("TC-AC-"+tc_id+"-DIM");
		targetModel.setType(deploymentTypeModel);
		targetModel.getVmInstances().add(vmi_1);
		targetModel.getVmInstances().add(vmi_2);
		targetModel.getVmInstances().add(vmi_3);
		targetModel.getVmInstances().add(vmi_4);
		//System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n"+ targetModel.getVmInstances());
		
		
		// Create CAMEL Model
		camel.core.CamelModel camelModel = camel.core.CoreFactory.eINSTANCE.createCamelModel();
		camelModel.setName("TC-AC-"+tc_id+"-CM");
		camelModel.getDeploymentModels().add(deploymentTypeModel);
		camelModel.getDeploymentModels().add(targetModel);
		camelModel.getLocationModels().add(locationModel);
		
		
		// Write CAMEL model to CDO
		eu.paasage.mddb.cdo.client.exp.CDOClientX cdoClient = new eu.paasage.mddb.cdo.client.exp.CDOClientXImpl();
		eu.paasage.mddb.cdo.client.exp.CDOSessionX cdoSessionX = cdoClient.getSession();
		org.eclipse.emf.cdo.transaction.CDOTransaction tr = cdoSessionX.openTransaction();
		
		String resourceName = "/test-cases/tc-ac-"+tc_id;
		org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject> contents = tr.getOrCreateResource(resourceName).getContents();
		contents.add(camelModel);
		tr.commit();
		System.out.println("OK");
	}
	
	public static void testPreauthorizeCamelModel(String tc_id) {
		String resourceName = "/test-cases/tc-ac-"+tc_id;
		
		eu.paasage.mddb.cdo.client.exp.CDOClientX cdoClient = new eu.paasage.mddb.cdo.client.exp.CDOClientXImpl();
		eu.paasage.mddb.cdo.client.exp.CDOSessionX cdoSessionX = cdoClient.getSession();
		org.eclipse.emf.cdo.transaction.CDOTransaction tr = cdoSessionX.openTransaction();
		
		//camel.deployment.DeploymentInstanceModel targetModel = cdoServerApi.getModelToDeploy(resourceName, tr);
		org.eclipse.emf.common.util.EList<org.eclipse.emf.ecore.EObject> contents = tr.getOrCreateResource(resourceName).getContents();
		camel.core.CamelModel model = (camel.core.CamelModel) contents.get(0);
		org.eclipse.emf.common.util.EList<camel.deployment.DeploymentModel> deploymentModels = model.getDeploymentModels();
		log.debug("Deployment models in CAMEL model: {}", resourceName);
		log.debug("{}", deploymentModels.stream().map(dm -> dm.getName()).collect(Collectors.toList()));
		camel.deployment.DeploymentInstanceModel targetModel = (camel.deployment.DeploymentInstanceModel) deploymentModels.get(1);
		log.info("Deployment-instance-model: {}", targetModel.getName());
		log.debug("VM-instances: {}", targetModel.getVmInstances().stream().map(vm -> vm.getName()).collect(Collectors.toList()));
		
		// pre-authorize target model
		if (targetModel!=null) {
            log.info("Authorizing deployment plan with Authorization-Service...");
			try {
				// Prepare auth.client properties
				String[] extractors = {
						"eu.melodic.upperware.adapter.extractor.CoreExtractor", "eu.melodic.upperware.adapter.extractor.RamExtractor", 
						"eu.melodic.upperware.adapter.extractor.StorageExtractor", "eu.melodic.upperware.adapter.extractor.CountryExtractor", 
						"eu.melodic.upperware.adapter.extractor.InstanceExtractor",
						"eu.melodic.upperware.adapter.extractor.ComponentExtractor",
						"eu.melodic.upperware.adapter.extractor.PerVmCoreExtractor", "eu.melodic.upperware.adapter.extractor.PerVmRamExtractor", 
						"eu.melodic.upperware.adapter.extractor.PerVmStorageExtractor", "eu.melodic.upperware.adapter.extractor.PerVmCountryExtractor",
						"eu.melodic.upperware.adapter.extractor.PerVmInstanceExtractor",
						"eu.melodic.upperware.adapter.extractor.CoreSetExtractor", "eu.melodic.upperware.adapter.extractor.RamSetExtractor",
						"eu.melodic.upperware.adapter.extractor.StorageSetExtractor", "eu.melodic.upperware.adapter.extractor.InstanceSetExtractor"
				};
				eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties.DataProperties dataProperties = 
						new eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties.DataProperties();
				dataProperties.setExtractors(extractors);
				
				eu.melodic.security.authorization.util.properties.HttpClientProperties httpClientProperties =
						new eu.melodic.security.authorization.util.properties.HttpClientProperties();
				httpClientProperties.setKeystoreFile("truststore-client.p12");
				httpClientProperties.setKeystoreFileType("PKCS12");
				httpClientProperties.setKeystorePassword("melodic");
				
				String[] pdpEndpoints = { "https://localhost:7071/checkJsonAccessRequest", "https://localhost:7072/checkJsonAccessRequest" };
				eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties.PdpConfig pdpProperties = 
						new eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties.PdpConfig();
				pdpProperties.setDisabled(false);
				pdpProperties.setEndpoints(pdpEndpoints);
				pdpProperties.setLoadBalanceMethod("ORDER");
				pdpProperties.setRetryCount(3);
				pdpProperties.setAccessKey("7235687126587231675321756752657236156321765723");
				pdpProperties.setHttpClient(httpClientProperties);
				
				eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties properties = 
						new eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties();
				properties.setPdp(pdpProperties);
				properties.setData(dataProperties);
				
				// Call data extractors to collect information from target model 
				eu.melodic.security.authorization.client.AuthorizationServiceClient authorizationServiceClient = 
						new eu.melodic.security.authorization.client.AuthorizationServiceClient(properties);
				eu.melodic.security.authorization.client.extractor.DataExtractorHelper helper = 
						new eu.melodic.security.authorization.client.extractor.DataExtractorHelper(authorizationServiceClient);
				//		new eu.melodic.security.authorization.client.extractor.DataExtractorHelper(properties);
				java.util.Map<String,Object> extra = helper.getModelData( targetModel );
				log.info("Extracted target model data: {}", extra);
				
				// Collect plan information to submit to (Pre-)Authorization Server
				String resource = "deployment-model";	//XXX: ...or the actual resource-id of target model in CDO
				String action = "DEPLOY";
				String subject = "Adapter";

				// Use PDP client to pre-authorize execution plan against policies
				try {
					log.info("Calling PDP: {}", authorizationServiceClient.getPdpEndpoint());
					boolean permit = authorizationServiceClient.requestAccess(resource, action, subject, extra);
					log.info("PDP decision: {}", (permit?"PERMIT":"DENY"));

					if (!permit)
						throw new RuntimeException("Plan pre-authorization has failed.");
				} catch (eu.melodic.security.authorization.client.AccessDeniedException adex) {
					log.error("Got AccessDeniedException: ", adex);
					log.error("PDP decision: DENY");
				} catch (Exception ex) {
					log.error("An error occurred while evaluating web access request: ", ex);
					throw new RuntimeException(ex);
				}
			} catch (Exception ex) {
			    log.error("Error: ", ex);
				//ex.printStackTrace(System.err);
			}
		} else {
			log.error("Cannot pre-authorize target model. Target model is null");
		}
	}
}