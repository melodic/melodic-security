/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context.repository;

import eu.melodic.security.authorization.context.ContextElement;
import eu.melodic.security.authorization.context.ContextRepositoryClient;
import eu.melodic.security.authorization.util.HttpClientUtil;
import eu.melodic.security.authorization.util.properties.ContextClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Collection;

@Slf4j
public class RestContextRepositoryClient implements ContextRepositoryClient {
	private RestTemplate restTemplate;
	private String repositoryEndpoint;
	private String accessKey;
	
	public void initialize(ContextClientProperties config) {
		// configure context client
		repositoryEndpoint = config.getRepositoryUrl().trim();
		accessKey = config.getAccessKey().trim();
		log.info("Context Repository endpoint:   {}", repositoryEndpoint);
		log.info("Context Repository access key: {}", accessKey!=null && !accessKey.isEmpty() ? "present" : "missing");
		
		// configure http client (used by context client and PDP client)
		try {
			restTemplate = HttpClientUtil.createRestTemplate(config.getHttpClient(), repositoryEndpoint);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	public void storeContextElement(ContextElement element) {
		Collection<ContextElement> c = new ArrayList<ContextElement>(1);
		c.add(element);
		storeContext(c);
	}
	
	public void storeContext(Collection<ContextElement> context) {
		log.trace("Context: {}", context);
		log.debug("Sending context to repository: {}", repositoryEndpoint);
		restTemplate.put(repositoryEndpoint, context);
		log.debug("Context sent to repository");
	}
	
	public long storeContextElementForRequest(ContextElement element, long requestId) {
		Collection<ContextElement> c = new ArrayList<ContextElement>(1);
		c.add(element);
		return storeContextForRequest(c, requestId);
	}
	
	public long storeContextForRequest(Collection<ContextElement> context, long requestId) {
		log.trace("Context: {}", context);
		log.debug("Sending context-for-request to repository: {}  Request: {}", repositoryEndpoint, requestId);
		requestId = restTemplate.postForObject(repositoryEndpoint+"/"+requestId, context, Long.class);
		return requestId;
	}
	
	public void deleteContextForRequest(long requestId) {
		log.debug("Sending clear-context-for-request to repository: {}  Request: {}", repositoryEndpoint, requestId);
		restTemplate.delete(repositoryEndpoint+"/"+requestId);
	}
}