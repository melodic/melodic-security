/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context.extractor;

import eu.melodic.security.authorization.context.ContextElement;
import eu.melodic.security.authorization.context.ContextExtractor;
import eu.melodic.security.authorization.context.ContextRepositoryClient;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class RandomContextExtractor implements ContextExtractor<Object> {
	
	private ContextRepositoryClient contextRepositoryClient;
	private int min = 1;
	private int max = 10;
	
	@Override
	public long extractContext(Object request) {
		return extractContextForRequest(-1, request);
	}
	
	@Override
	public long extractContextForRequest(long requestId, Object request) {
		log.debug("**********  RandomContextExtractor invoked  **********");
		
		// Generate random context elements
		int howmany = (int)(Math.random()*(max-min))+min;
		log.info("Generating {} random context elements...", howmany);
		
		List<ContextElement> context = new ArrayList<ContextElement>();
		String keyBase = java.util.UUID.randomUUID().toString();
		for (int i=0; i<howmany; i++) {
			String key = keyBase+"---"+i;
			String value = java.util.UUID.randomUUID().toString();
			context.add(new ContextElement<String>(key, value));
		}
		log.info("Generated random context: {}", context);
		
		// Save context into the configured context store
		if (contextRepositoryClient!=null) {
			log.debug("Storing request context with client: {}", contextRepositoryClient.getClass().getName());
			log.debug("Storing request context with request-id: {}", requestId);
			requestId = contextRepositoryClient.storeContextForRequest(context, requestId);
			log.debug("Request context stored and got request-id: {}", requestId);
		}
		return requestId;
	}
	
	@Override
	public long[] extractContext(Object... requests) {
		return extractContextForRequest(-1, requests);
	}
	
	@Override
	public long[] extractContextForRequest(long requestId, Object... requests) {
		log.debug("**********  RandomContextExtractor invoked  **********");
		long[] requestIds = new long[ requests.length ];
		for (int i=0, n=requests.length; i<n; i++) {
			requestIds[i] = extractContextForRequest(requestId, requests[i]);
		}
		return requestIds;
	}
	
	@Override
	public ContextRepositoryClient getContextRepositoryClient() { return this.contextRepositoryClient; }
	
	@Override
	public void setContextRepositoryClient(ContextRepositoryClient client) { this.contextRepositoryClient = client; }
}