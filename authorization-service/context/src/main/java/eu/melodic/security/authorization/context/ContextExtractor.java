/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context;

public interface ContextExtractor<T> {
	long extractContext(T arg);
	long[] extractContext(T... args);
	
	long extractContextForRequest(long requestId, T arg);
	long[] extractContextForRequest(long requestId, T... args);
	
	ContextRepositoryClient getContextRepositoryClient();
	void setContextRepositoryClient(ContextRepositoryClient client);
}