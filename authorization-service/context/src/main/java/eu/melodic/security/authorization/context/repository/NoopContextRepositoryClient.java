/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context.repository;

import eu.melodic.security.authorization.context.ContextElement;
import eu.melodic.security.authorization.context.ContextRepositoryClient;
import eu.melodic.security.authorization.util.properties.ContextClientProperties;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;

@Slf4j
public class NoopContextRepositoryClient implements ContextRepositoryClient {
	public void initialize(ContextClientProperties config) {
		log.info("Context client properties: {}", config);
	}
	
	public void storeContextElement(ContextElement element) {
		Collection<ContextElement> c = new ArrayList<ContextElement>(1);
		c.add(element);
		storeContext(c);
	}
	
	public void storeContext(Collection<ContextElement> context) {
		log.info("Context: {}", context);
	}
	
	public long storeContextElementForRequest(ContextElement element, long requestId) {
		storeContextElement(element);
		return requestId;
	}
	
	public long storeContextForRequest(Collection<ContextElement> context, long requestId) {
		log.info("Context-for-Request: {}  Request: {}", context, requestId);
		return requestId;
	}
	
	public void deleteContextForRequest(long requestId) {
		log.info("Clearing context-for-request: id={}", requestId);
	}
}