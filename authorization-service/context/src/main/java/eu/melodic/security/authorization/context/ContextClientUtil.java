/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context;

import eu.melodic.security.authorization.util.properties.ContextClientProperties;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ContextClientUtil {
	public static ContextExtractor createExtractor(ContextClientProperties properties) {
		if (properties!=null) {
			boolean extractContext = properties.isEnable();
			String repositoryClientClass = properties.getRepositoryClientClass();
			java.util.List<String> extractorClasses = properties.getExtractorClasses();
			for (String s : extractorClasses) System.out.println(s);
			log.info("Context extraction config: enable={}, repos-client={}, extractor={}", extractContext, repositoryClientClass, extractorClasses);
			if (extractContext) {
				// initialize context repository client
				ContextRepositoryClient contextReposClient = ContextRepositoryClientFactory.getContextRepositoryClient(repositoryClientClass);
				//contextReposClient = ContextRepositoryClientFactory.getContextRepositoryClient(RestContextRepositoryClient.class);
				contextReposClient.initialize(properties);
				
				// initialize client-side context extractor
				ContextExtractor extractor = ContextExtractorFactory.getContextExtractor(extractorClasses.get(0));
				//extractor = (HttpRequestContextExtractor) ContextExtractorFactory.getContextExtractor(HttpRequestContextExtractor.class);
				extractor.setContextRepositoryClient(contextReposClient);
				
				return extractor;
			}
		}
		return null;
	}
}