/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context;

public class ContextExtractorFactory {
	public static ContextExtractor getContextExtractor(String name) throws IllegalArgumentException {
		Class clazz = null;
		try {
			clazz = Class.forName(name);
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
		return getContextExtractor(clazz);
	}
	
	public static ContextExtractor getContextExtractor(Class clazz) throws IllegalArgumentException {
		try {
			return (ContextExtractor) clazz.newInstance();
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
	}
}