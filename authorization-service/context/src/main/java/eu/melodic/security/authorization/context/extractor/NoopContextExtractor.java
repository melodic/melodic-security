/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context.extractor;

import eu.melodic.security.authorization.context.ContextExtractor;
import eu.melodic.security.authorization.context.ContextRepositoryClient;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class NoopContextExtractor implements ContextExtractor<Object> {
	@Override
	public long extractContext(Object arg) {
		log.debug("extractContext for: {}", arg);
		return -1;
	}
	@Override
	public long[] extractContext(Object... args) {
		log.debug("extractContext for: {}", args);
		return null;
	}
	
	@Override
	public long extractContextForRequest(long requestId, Object arg) {
		log.debug("extractContext for request: id={}, context={}", requestId, arg);
		return -1;
	}
	@Override
	public long[] extractContextForRequest(long requestId, Object... args) {
		log.debug("extractContext for request: id={}, context={}", requestId, args);
		return null;
	}
	
	@Override
	public ContextRepositoryClient getContextRepositoryClient() {
		log.debug("getContextRepositoryClient");
		return null;
	}
	
	@Override
	public void setContextRepositoryClient(ContextRepositoryClient client) {
		log.debug("setContextRepositoryClient");
	}
}