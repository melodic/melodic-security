/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context;

import eu.melodic.security.authorization.util.properties.ContextClientProperties;

import java.util.Collection;

public interface ContextRepositoryClient {
	void initialize(ContextClientProperties properties);
	void storeContextElement(ContextElement element);
	void storeContext(Collection<ContextElement> context);
	long storeContextElementForRequest(ContextElement element, long requestId);
	long storeContextForRequest(Collection<ContextElement> context, long requestId);
	void deleteContextForRequest(long requestId);
}