/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context;

public class ContextRepositoryClientFactory {
	public static ContextRepositoryClient getContextRepositoryClient(String name) throws IllegalArgumentException {
		Class clazz = null;
		try {
			clazz = Class.forName(name);
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
		return getContextRepositoryClient(clazz);
	}
	
	public static ContextRepositoryClient getContextRepositoryClient(Class<? extends ContextRepositoryClient> clazz) throws IllegalArgumentException {
		try {
			return clazz.newInstance();
		} catch (Exception ex) {
			throw new IllegalArgumentException(ex);
		}
	}
}