/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context;

import lombok.Getter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Getter
@ToString
@Slf4j
public class ContextElement<T> {
	public final static String _DEFAULT_SCOPE = "_default_";
	
	private String name;
	private T value;
	private String scope;
	private long timestamp;
	private long requestId;
	
	public ContextElement() {}
	
	public ContextElement(String name, T value) {
		this(name, value, _DEFAULT_SCOPE, System.currentTimeMillis(), -1);
	}
	
	public ContextElement(String name, T value, String scope) {
		this(name, value, scope, System.currentTimeMillis(), -1);
	}
	
	public ContextElement(String name, T value, long timestamp) {
		this(name, value, _DEFAULT_SCOPE, timestamp, -1);
	}
	
	public ContextElement(String name, T value, long timestamp, long requestId) {
		this(name, value, _DEFAULT_SCOPE, timestamp, requestId);
	}
	
	public ContextElement(String name, T value, String scope, long timestamp) {
		this(name, value, scope, timestamp, -1);
	}
	
	public ContextElement(String name, T value, String scope, long timestamp, long requestId) {
		this.name = name;
		this.value = value;
		this.scope = scope;
		this.timestamp = timestamp;
		this.requestId = requestId;
	}
}