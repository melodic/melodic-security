/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.context.extractor;

import eu.melodic.security.authorization.context.ContextElement;
import eu.melodic.security.authorization.context.ContextExtractor;
import eu.melodic.security.authorization.context.ContextRepositoryClient;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Slf4j
public abstract class HttpRequestContextExtractor implements ContextExtractor<HttpServletRequest> {
	
	private ContextRepositoryClient contextRepositoryClient;
	
	protected abstract void doContextExtraction(HttpServletRequest request, List<ContextElement> context);
	
	@Override
	public long extractContext(HttpServletRequest request) {
		return extractContextForRequest(-1, request);
	}
	
	@Override
	public long extractContextForRequest(long requestId, HttpServletRequest request) {
		log.debug("**********  HttpRequestContextExtractor invoked  **********");
		
		// Context extraction to occur here
		log.debug("Extracting request context");
		List<ContextElement> context = new ArrayList<ContextElement>();
		doContextExtraction(request, context);
		log.debug("Request context extracted: {}", context);
		
		// Save context into the configured context store
		if (contextRepositoryClient!=null) {
			log.debug("Storing request context with client: {}", contextRepositoryClient.getClass().getName());
			log.debug("Storing request context with request-id: {}", requestId);
			requestId = contextRepositoryClient.storeContextForRequest(context, requestId);
			log.debug("Request context stored and got request-id: {}", requestId);
		}
		return requestId;
	}
	
	protected Map<String,String> prepareParameterMap(Map parameters) {
		Map<String,String> map = new HashMap<String,String>();
		for (Object name : parameters.keySet()) {
			Object value = parameters.get(name);
			String nameStr  = name!=null ? name.toString() : null;
			String valueStr = null;
			if (value!=null) {
				valueStr = value.getClass().isArray()
						? Arrays.toString( (Object[])value )
						: value.toString();
			}
			map.put(nameStr, valueStr);
		}
		return map;
	}
	
	@Override
	public long[] extractContext(HttpServletRequest... requests) {
		return extractContextForRequest(-1, requests);
	}
	
	@Override
	public long[] extractContextForRequest(long requestId, HttpServletRequest... requests) {
		log.info("**********  HttpRequestContextExtractor invoked  **********");
		long[] requestIds = new long[ requests.length ];
		for (int i=0, n=requests.length; i<n; i++) {
			requestIds[i] = extractContextForRequest(requestId, requests[i]);
		}
		return requestIds;
	}
	
	@Override
	public ContextRepositoryClient getContextRepositoryClient() { return this.contextRepositoryClient; }
	
	@Override
	public void setContextRepositoryClient(ContextRepositoryClient client) { this.contextRepositoryClient = client; }
}