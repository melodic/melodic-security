/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.util.json;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.*;

@Slf4j
public class JsonSerializer {
	// Basic types
	public static final List<String> basicTypes = Arrays.asList(
		"java.lang.Boolean", "java.lang.String", "java.lang.Float", "java.lang.Double",
		"java.lang.Byte", "java.lang.Short", "java.lang.Integer", "java.lang.Long"
	);
	// W3C date-time format
	public static final java.text.SimpleDateFormat w3cFmt = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
	// Delimeters used in JSON messages
	public static final String[] DELIMETERS = new String[]{":",",","{","}","[","]"};
	public static final String[] DELIMETER_REPLACEMENTS = new String[]{"\\u003A","\\u002C","\\u007B","\\u007D","\\u005B","\\u005D"};
	
	/**
	 * Limitations:
	 * This class is not a full object graph serializer. The same object referenced two or more times will be serialized multiple times.
	 * Maps are assumed to have String keys and Object values. Collections are assumed to have Object elements.
	 */
	public String serializeObject(Object o) {
		StringBuilder sb = new StringBuilder();
		_serializeObject(sb, 0, o);
		sb.append("\n");
		return sb.toString();
	}
	
	// --------------------------------------------------------------------------------------------
	
	protected void _serializeObject(StringBuilder sb, int identation, Object o) {
		if (o==null) {
			sb.append("{ \"type\": null, \"value\": null }");
		} else
		if (o instanceof Map) {
			_serializeMap(sb, identation, (Map)o);
		} else
		if (o instanceof Collection) {
			_serializeCollection(sb, identation, (Collection)o);
		} else
		if (o.getClass()!=null && o.getClass().isArray()) {
			_serializeArray(sb, identation, o);
		} else
		{
			_serializeObjectFields(sb, identation, o);
		}
	}
	
	protected void _serializeMap(StringBuilder sb, int identation, Map map) {
		sb.append("{\n");
		ident(sb, identation+1);	sb.append("\"type\": \"map\",\n");
		ident(sb, identation+1);	sb.append("\"class\": \"").append(jsStr(map.getClass().getName())).append("\",\n");
		ident(sb, identation+1);	sb.append("\"value\": {\n");
		map.forEach((k,v)->{
			ident(sb, identation+2);
			if (k!=null)	sb.append("\"").append(jsStr(k.toString())).append("\": ");
			else			sb.append("null: ");
			_serializeObject(sb, identation+2, v);
			sb.append(",\n");
		});
		ident(sb, identation+1);	sb.append("}\n");
		ident(sb, identation);		sb.append("}");
	}
	
	protected void _serializeCollection(StringBuilder sb, int identation, Collection col) {
		sb.append("{\n");
		ident(sb, identation+1);	sb.append("\"type\": \"array\",\n");
		ident(sb, identation+1);	sb.append("\"class\": ").append(jsStr(col.getClass().getName())).append(",\n");
		ident(sb, identation+1);	sb.append("\"value\": [\n");
		col.forEach(v->{
			ident(sb, identation+2);
			_serializeObject(sb, identation+2, v);
			sb.append(",\n");
		});
		ident(sb, identation+1);	sb.append("]\n");
		ident(sb, identation);		sb.append("}");
	}
	
	protected void _serializeArray(StringBuilder sb, int identation, Object arr) {
		sb.append("{\n");
		ident(sb, identation+1);	sb.append("\"type\": \"array\",\n");
		ident(sb, identation+1);	sb.append("\"class\": ").append(jsStr(arr.getClass().getComponentType().getName())).append(",\n");
		int length = Array.getLength(arr);
		ident(sb, identation+1);	sb.append("\"length\": ").append( length ).append(",\n");
		ident(sb, identation+1);	sb.append("\"value\": [\n");
		for (int i=0; i<length; i++) {
			ident(sb, identation+2);
			_serializeObject(sb, identation+2, Array.get(arr, i));
			sb.append(",\n");
		}
		ident(sb, identation+1);	sb.append("]\n");
		ident(sb, identation);		sb.append("}");
	}
	
	protected void _serializeObjectFields(StringBuilder sb, int identation, Object o) {
		if (o==null) {
			sb.append("{ \"type\": null, \"value\": null }");
		} else {
			String clazz = o.getClass().getName();
			if (basicTypes.contains(clazz)) {
				sb.append("{\n");
				ident(sb, identation+1);	sb.append("\"type\": \"").append( jsStr(clazz) ).append("\",\n");
				ident(sb, identation+1);	sb.append("\"value\": \"").append( jsStr(o.toString()) ).append("\"\n");
				ident(sb, identation);		sb.append("}");
			} else
			if (clazz.equals("java.util.Date")) {
				sb.append("{\n");
				ident(sb, identation+1);	sb.append("\"type\": \"").append( jsStr(clazz) ).append("\",\n");
				ident(sb, identation+1);	sb.append("\"value\": \"").append( toW3cFormat((Date)o) ).append("\"\n");
				ident(sb, identation);		sb.append("}");
			} else 
			{
				sb.append("{\n");
				ident(sb, identation+1);	sb.append("\"type\": \"").append( jsStr(clazz) ).append("\",\n");
				ident(sb, identation+1);	sb.append("\"value\": {\n");
				for (Field field : getObjectFields(o.getClass(), new Vector<Field>())) {
					String fieldName = field.getName();
					Object fieldValue = null;
					int modifiers = field.getModifiers();
					boolean isTransient = (modifiers & Modifier.TRANSIENT) != 0;
					//boolean isFinal = (modifiers & Modifier.FINAL) != 0;
					boolean isFinal = false;
					if (!isTransient & !isFinal) {
						try {
							//boolean isPrivate = (modifiers & Modifier.PRIVATE) != 0;
							//boolean isProtected = (modifiers & Modifier.PROTECTED) != 0;
							//boolean isPublic = (modifiers & Modifier.PUBLIC) != 0;
							boolean isAccessible = field.isAccessible();
							if (!isAccessible) field.setAccessible(true);
							fieldValue = field.get(o);
							if (!isAccessible) field.setAccessible(false);
						} catch (IllegalAccessException iae) {
							throw new RuntimeException("Could not read value of field: "+clazz+"."+fieldName, iae);
						}
						ident(sb, identation+2);	sb.append("\".").append( jsStr(fieldName) ).append("\": ");
						_serializeObject(sb, identation+2, fieldValue);
						sb.append(",\n");
					}
				}
				ident(sb, identation+1);	sb.append("}\n");
				ident(sb, identation);		sb.append("}");
			}
		}
	}
	
	protected List<Field> getObjectFields(Class clazz, Vector<Field> vector) {
		Class superClazz = clazz.getSuperclass();
		if(superClazz != null){
			getObjectFields(superClazz, vector);
		}
		vector.addAll(Arrays.asList(clazz.getDeclaredFields()));
		return vector;
	}
	
	protected String toW3cFormat(Date d) {
		return w3cFmt.format(d)+":00";
	}
	
	protected void ident(StringBuilder sb, int identation) {
		for (int i=0; i<identation; i++) sb.append( "  " );
	}
	
	protected String jsStr(String s) {
		s = StringEscapeUtils.escapeEcmaScript(s);
		s = StringUtils.replaceEachRepeatedly(s, DELIMETERS, DELIMETER_REPLACEMENTS);
		return s;
	}
}
