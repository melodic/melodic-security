/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.util.json;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.*;

@Slf4j
public class JsonUnserializer {
	// Tokens used
	protected final static String OBJECT_OPEN  = "{";
	protected final static String OBJECT_CLOSE = "}";
	protected final static String ARRAY_OPEN  = "[";
	protected final static String ARRAY_CLOSE = "]";
	protected final static String COLON = ":";
	protected final static String COMMA = ",";
	// Errors
	protected final static String TYPE_NOT_FOUND = "TYPE_NOT_FOUND";
	protected final static String TYPE_CLASS_NOT_FOUND = "TYPE_CLASS_NOT_FOUND";
	protected final static String VALUE_NOT_FOUND = "VALUE_NOT_FOUND";
	protected final static String UNEXPECTED_VALUE = "UNEXPECTED_VALUE";
	// Error messages
	protected final static Map<String,String> exceptionMessages =
		Collections.unmodifiableMap(new HashMap<String,String>() {{ 
			put(OBJECT_OPEN, "JsonUnserializer: OBJECT_OPEN not found. Current token: %s");
			put(OBJECT_CLOSE, "JsonUnserializer: OBJECT_CLOSE not found. Current token: %s");
			put(ARRAY_OPEN, "JsonUnserializer: ARRAY_OPEN not found. Current token: %s");
			put(ARRAY_CLOSE, "JsonUnserializer: ARRAY_CLOSE not found. Current token: %s");
			put(COLON, "JsonUnserializer: COLON not found. Current token: %s");
			put(COMMA, "JsonUnserializer: COMMA not found. Current token: %s");
			put(TYPE_NOT_FOUND, "JsonUnserializer: TYPE specification not found. Current token: %s");
			put(TYPE_CLASS_NOT_FOUND, "JsonUnserializer: TYPE-CLASS specification not found. Current token: %s");
			put(VALUE_NOT_FOUND, "JsonUnserializer: VALUE specification not found. Current token: %s");
			put(UNEXPECTED_VALUE, "JsonUnserializer: UNEXPECTED VALUE encountered: %s");
		}});
	// Delimeters
	protected final static String DELIMETERS_ALL = "{[:,]}";
	protected final static String DELIMETERS_DATE = "}";
	
	// Look-ahead cache
	protected String _nextToken = null;
	
	/**
	 * Limitations:
	 * This class is not a full object graph unserializer. It will unserialize the same object multiple times (since it does not support references).
	 * Maps are assumed to have String keys and Object values. Collections are assumed to have Object elements.
	 * Objects being unserialized must have no argument constructors.
	 */
	public Object unserializeObject(String s) {
		StringTokenizer st = new StringTokenizer(s, DELIMETERS_ALL, true);
		return _unserializeObject(st);
	}
	
	// --------------------------------------------------------------------------------------------
	
	protected void checkToken(StringTokenizer st, String delim) {
		String token = _nextToken!=null ? _nextToken : st.nextToken(DELIMETERS_ALL).trim();
		_nextToken = null;
		if (token.equals( delim )) return;
		throw new RuntimeException( String.format( exceptionMessages.get(delim), token ) );
	}
	
	protected String getObjectType(StringTokenizer st) {
		String token = _nextToken!=null ? _nextToken : st.nextToken(DELIMETERS_ALL).trim();
		_nextToken = null;
		if (token.equalsIgnoreCase("\"type\"")) {
			checkToken(st, COLON);
			String type = unjsStr( st.nextToken().trim() );
			// Consume next comma
			checkToken(st, COMMA);
			return type;
		} else
			throw new RuntimeException( String.format( exceptionMessages.get(TYPE_NOT_FOUND), token ) );
	}
	
	protected String getTypeClass(StringTokenizer st) {
		String token = _nextToken!=null ? _nextToken : st.nextToken(DELIMETERS_ALL).trim();
		_nextToken = null;
		if (token.equalsIgnoreCase("\"class\"")) {
			checkToken(st, COLON);
			String typeClass = unjsStr( st.nextToken().trim() );
			// Consume next comma
			checkToken(st, COMMA);
			return typeClass;
		} else
			throw new RuntimeException( String.format( exceptionMessages.get(TYPE_CLASS_NOT_FOUND), token ) );
	}
	
	protected void checkValue(StringTokenizer st) {
		String token = _nextToken!=null ? _nextToken : st.nextToken(DELIMETERS_ALL).trim();
		_nextToken = null;
		if (token.equalsIgnoreCase("\"value\"")) {
			// Consume next colon
			checkToken(st, COLON);
		} else
			throw new RuntimeException( String.format( exceptionMessages.get(VALUE_NOT_FOUND), token ) );
	}
	
	protected int getLength(StringTokenizer st) {
		String token = _nextToken!=null ? _nextToken : st.nextToken(DELIMETERS_ALL).trim();
		_nextToken = null;
		if (token.equalsIgnoreCase("\"length\"")) {
			// Consume next colon
			checkToken(st, COLON);
			// Get length value
			String v = nextToken(st);
			// Consume next comma
			consumeComma(st);
			
			return Integer.parseInt(v);
		} else {
			_nextToken = token;
		}
		return -1;
	}
	
	protected String nextToken(StringTokenizer st) {
		String token = _nextToken!=null ? _nextToken : st.nextToken(DELIMETERS_ALL).trim();
		_nextToken = null;
		return token;
	}
	
	protected boolean nextTokenEquals(StringTokenizer st, String value) {
		//log.debug("nextTokenEquals:  checking for value: {}", value);
		//log.debug("nextTokenEquals:  _nextToken: {}", _nextToken);
		String token = _nextToken!=null ? _nextToken : st.nextToken(DELIMETERS_ALL).trim();
		//log.debug("nextTokenEquals:  token: {}", token);
		if (token.isEmpty()) {
			//log.debug("nextTokenEquals:  consuming white spaces");
			consumeWhitespaces(st);
			//log.debug("nextTokenEquals:  AFTER consuming white spaces -- _nextToken={}", _nextToken);
			token = _nextToken!=null ? _nextToken : st.nextToken(DELIMETERS_ALL).trim();
			//log.debug("nextTokenEquals:  AFTER consuming white spaces -- token={}", token);
		}
		if (_nextToken==null) _nextToken = token;
		//log.debug("nextTokenEquals:  NEW _nextToken: {}", _nextToken);
		//log.debug("nextTokenEquals:  RETURN: {}", token.equals(value));
		return token.equals(value);
	}
	
	protected void consumeComma(StringTokenizer st) {
		if (st.hasMoreTokens() || _nextToken!=null) {
			//log.debug("---------->  ........  Consuming trailing comma");
			String token = _nextToken!=null ? _nextToken : st.nextToken(DELIMETERS_ALL).trim();
			_nextToken = (! token.equals( COMMA )) ? token : null;
		}
	}
	
	protected void consumeWhitespaces(StringTokenizer st) {
		if (st.hasMoreTokens() || _nextToken!=null) {
			//log.debug("---------->  ........  Consuming whitespaces");
			String token = _nextToken!=null ? _nextToken : st.nextToken(DELIMETERS_ALL).trim();
			_nextToken = (! token.isEmpty()) ? token : null;
		}
	}
	
	protected Object _unserializeObject(StringTokenizer st) {
		//log.debug("---------->  BEFORE OBJECT_OPEN");
		consumeWhitespaces(st);
		checkToken(st, OBJECT_OPEN);
		//log.debug("---------->  AFTER OBJECT_OPEN");
		String type = getObjectType(st);
		//log.debug("---------->  GOT TYPE: "+type);
		String typeClass = null;
		if (type.equals("map") || type.equals("array")) {
			typeClass = getTypeClass(st);
			//log.debug("---------->  AFTER CLASS: "+typeClass);
		}
		int length = getLength(st);
		//log.debug("---------->  AFTER LENGTH: "+length);
		checkValue(st);
		//log.debug("---------->  AFTER VALUE");
		Object object = null;
		if (type.equals("null")) {
			//log.debug("---------->  TYPE is 'null'");
			object = null;
			String value = st.nextToken().trim();
			if (! value.equals("null"))
				throw new RuntimeException( String.format( exceptionMessages.get(UNEXPECTED_VALUE), value) );
		} else
		if (type.equals("map")) {
			//log.debug("---------->  TYPE is 'map'");
			object = _unserializeMap(st, typeClass);
		} else
		if (type.equals("array")) {
			if (length==-1) {
				//log.debug("---------->  TYPE is 'array'");
				object = _unserializeCollection(st, typeClass);
			} else {
				//log.debug("---------->  TYPE is 'array[{}]'", length);
				object = _unserializeArray(st, typeClass, length);
			}
		} else
		{
			//log.debug("---------->  TYPE is OBJECT");
			object = _unserializeObjectFields(st, type);
		}
		//log.debug("---------->  BEFORE OBJECT_CLOSE");
		consumeWhitespaces(st);
		checkToken(st, OBJECT_CLOSE);
		//log.debug("---------->  AFTER OBJECT_CLOSE");
		// Consume any trailing comma
		consumeComma(st);
		
		return object;
	}
	
	protected Map<String,Object> _unserializeMap(StringTokenizer st, String typeClass) {
		// Instantiate map
		//log.debug("---------->  _unserializeMap:  Instantiate map of type="+typeClass);
		Map<String,Object> map = null;
		Class clazz = null;
		try {
			clazz = Class.forName(typeClass);
			map = (Map<String,Object>)clazz.newInstance();
			//log.debug("---------->  _unserializeMap:  Instantiate map of type="+typeClass+": ok");
		} catch (Exception e) {
			throw new RuntimeException( String.format( exceptionMessages.get(UNEXPECTED_VALUE), typeClass) + ". Type not found: "+typeClass, e );
		}
		
		// Consume map's opening tag
		//log.debug("---------->  ------> MAP-VALUES: BEFORE OBJECT_OPEN");
		consumeWhitespaces(st);
		checkToken(st, OBJECT_OPEN);
		//log.debug("---------->  ------> MAP-VALUES: AFTER OBJECT_OPEN");
		
		// Process map entries
		while (! nextTokenEquals(st, OBJECT_CLOSE))
		{
			// get entry key
			//log.debug("---------->  ------> MAP-VALUES: BEFORE ENTRY-key");
			String key = nextToken(st);
			//log.debug("---------->  ------> MAP-VALUES: AFTER ENTRY-key: "+key);
			if (key.equals("null")) key = null;
			else key = unjsStr( key );
			//log.debug("---------->  ------> MAP-VALUES: OK ENTRY-key: "+key);
			// Consume next colon
			checkToken(st, COLON);
			
			// get entry value
			//log.debug("---------->  ------> MAP-VALUES: BEFORE ENTRY-value");
			Object value = _unserializeObject(st);
			//log.debug("---------->  ------> MAP-VALUES: AFTER ENTRY-value");
			
			// Consume next comma
			//consumeWhitespaces(st);
			consumeComma(st);
			
			// put entry in map
			//log.debug("---------->  ------> MAP-VALUES: BEFORE PUTTING entry in map: "+map);
			map.put(key, value);
			//log.debug("---------->  ------> MAP-VALUES: AFTER PUTTING entry in map: "+map);
		}
		
		// Consume map's closing tag
		//log.debug("---------->  ------> MAP-VALUES: BEFORE OBJECT_CLOSE");
		consumeWhitespaces(st);
		checkToken(st, OBJECT_CLOSE);
		//log.debug("---------->  ------> MAP-VALUES: AFTER OBJECT_CLOSE");
		// Consume any trailing comma
		consumeComma(st);
		
		return map;
	}
	
	protected Collection<Object> _unserializeCollection(StringTokenizer st, String typeClass) {
		// Instantiate collection
		//log.debug("---------->  _unserializeCollection:  Instantiate collection of type="+typeClass);
		Collection<Object> col = null;
		Class clazz = null;
		try {
			clazz = Class.forName(typeClass);
			col = (Collection<Object>)clazz.newInstance();
			//log.debug("---------->  _unserializeCollection:  Instantiate collection of type="+typeClass+": ok");
		} catch (Exception e) {
			throw new RuntimeException( String.format( exceptionMessages.get(UNEXPECTED_VALUE), typeClass) + ". Type not found: "+typeClass, e );
		}
		
		// Consume collection's opening tag
		//log.debug("---------->  ------> COLLECTION-VALUES: BEFORE ARRAY_OPEN");
		consumeWhitespaces(st);
		checkToken(st, ARRAY_OPEN);
		//log.debug("---------->  ------> COLLECTION-VALUES: AFTER ARRAY_OPEN");
		
		// Process collection elements
		while (! nextTokenEquals(st, ARRAY_CLOSE))
		{
			// get element
			//log.debug("---------->  ------> COLLECTION-VALUES: BEFORE ELEMENT-value");
			Object element = _unserializeObject(st);
			//log.debug("---------->  ------> COLLECTION-VALUES: AFTER ELEMENT-value");
			
			// Consume next comma
			//consumeWhitespaces(st);
			consumeComma(st);
			
			// add element in collection
			//log.debug("---------->  ------> COLLECTION-VALUES: BEFORE ADDING element in collection: "+col);
			col.add(element);
			//log.debug("---------->  ------> COLLECTION-VALUES: AFTER ADDING element in collection: "+col);
		}
		
		// Consume collection's closing tag
		//log.debug("---------->  ------> COLLECTION-VALUES: BEFORE ARRAY_CLOSE");
		consumeWhitespaces(st);
		checkToken(st, ARRAY_CLOSE);
		//log.debug("---------->  ------> COLLECTION-VALUES: AFTER ARRAY_CLOSE");
		// Consume any trailing comma
		consumeComma(st);
		
		return col;
	}
	
	protected Object _unserializeArray(StringTokenizer st, String typeClass, int length) {
		// Instantiate array
		//log.debug("---------->  _unserializeArray:  Instantiate array of type="+typeClass+", length="+length);
		Object arr = null;
		Class clazz = null;
		try {
			clazz = Class.forName(typeClass);
			arr = Array.newInstance(clazz, length);
			//log.debug("---------->  _unserializeArray:  Instantiate array of type="+typeClass+", length="+length+": ok");
		} catch (Exception e) {
			throw new RuntimeException( String.format( exceptionMessages.get(UNEXPECTED_VALUE), typeClass) + ". Type not found or wrong array size: "+typeClass+", "+length, e );
		}
		
		// Consume array's opening tag
		//log.debug("---------->  ------> ARRAY-VALUES: BEFORE ARRAY_OPEN");
		consumeWhitespaces(st);
		checkToken(st, ARRAY_OPEN);
		//log.debug("---------->  ------> ARRAY-VALUES: AFTER ARRAY_OPEN");
		
		// Process array elements
		int index = 0;
		while (! nextTokenEquals(st, ARRAY_CLOSE))
		{
			// get element
			//log.debug("---------->  ------> ARRAY-VALUES: BEFORE ELEMENT-value");
			Object element = _unserializeObject(st);
			//log.debug("---------->  ------> ARRAY-VALUES: AFTER ELEMENT-value");
			
			// Consume next comma
			//consumeWhitespaces(st);
			consumeComma(st);
			
			// add element in array
			//log.debug("---------->  ------> ARRAY-VALUES: BEFORE ADDING element in array: "+Arrays.toString(arr));
			Array.set(arr, index++, element);
			//log.debug("---------->  ------> ARRAY-VALUES: AFTER ADDING element in array: "+Arrays.toString(arr));
		}
		
		// Consume arrays's closing tag
		//log.debug("---------->  ------> ARRAY-VALUES: BEFORE ARRAY_CLOSE");
		consumeWhitespaces(st);
		checkToken(st, ARRAY_CLOSE);
		//log.debug("---------->  ------> ARRAY-VALUES: AFTER ARRAY_CLOSE");
		// Consume any trailing comma
		consumeComma(st);
		
		return arr;
	}
	
	protected Object _unserializeObjectFields(StringTokenizer st, String type) {
		//log.debug("---------->  _unserializeObjectFields:  type="+type);
		Object object = null;
		if (JsonSerializer.basicTypes.contains(type)) {
			//log.debug("---------->  _unserializeObjectFields:  BASIC-TYPE="+type);
			// get value for basic type
			String value = unjsStr( nextToken(st) );
			//log.debug("---------->  _unserializeObjectFields:  VALUE="+value);
			
			// instantiate basic type object
			if ("java.lang.Boolean".equals(type)) object = new Boolean(value);
			else if ("java.lang.String".equals(type)) object = new String(value);
			else if ("java.lang.Float".equals(type)) object = new Float(value);
			else if ("java.lang.Double".equals(type)) object = new Double(value);
			else if ("java.lang.Byte".equals(type)) object = new Byte(value);
			else if ("java.lang.Short".equals(type)) object = new Short(value);
			else if ("java.lang.Integer".equals(type)) object = new Integer(value);
			else if ("java.lang.Long".equals(type)) object = new Long(value);
			else
				throw new RuntimeException("JsonUnserializer: FATAL: BUG: 'basicTypes' contains more basic types or not all basic types have been implemented in JsonUnserializer");
			//log.debug("---------->  _unserializeObjectFields:  OBJECT="+object);
		} else
		if (type.equals("java.util.Date")) {
			//log.debug("---------->  _unserializeObjectFields:  DATE-TYPE="+type);
			// get value for basic type
			String value = unjsStr( st.nextToken(DELIMETERS_DATE).trim() );
			//log.debug("---------->  _unserializeObjectFields:  DATE-VALUE="+value);
			
			// instantiate basic type object
			object = fromW3cFormat(value);
		} else
		{
			// Instantiate object
			//log.debug("---------->  _unserializeObjectFields:  Instantiate object of type="+type);
			Class clazz = null;
			Map<String,Field> fields = null;
			try {
				clazz = Class.forName(type);
				object = clazz.newInstance();
				fields = getObjectFields(clazz, new HashMap<String,Field>());
				//log.debug("---------->  _unserializeObjectFields:  Instantiate object of type="+type+": ok");
			} catch (Exception e) {
				throw new RuntimeException( String.format( exceptionMessages.get(UNEXPECTED_VALUE), type) + ". Type not found: "+type, e );
			}
			
			// Consume object's opening tag
			//log.debug("---------->  _unserializeObjectFields:  Looking for OBJECT-VALUES opening tag. has-more-token="+st.hasMoreTokens());
			//log.debug("---------->  ------> OBJECT-VALUES: BEFORE OBJECT_OPEN");
			consumeWhitespaces(st);
			checkToken(st, OBJECT_OPEN);
			//log.debug("---------->  ------> OBJECT-VALUES: AFTER OBJECT_OPEN");
			
			// Process object fields
			while (! nextTokenEquals(st, OBJECT_CLOSE))
			{
				// get field name
				//log.debug("---------->  ------> OBJECT-VALUES: BEFORE field-name");
				String fieldName = unjsStr( nextToken(st) );
				//log.debug("---------->  ------> OBJECT-VALUES: AFTER field-name: "+fieldName);
				if (fieldName.startsWith(".")) {
					fieldName = fieldName.substring(1);
				} else
					throw new RuntimeException( String.format( exceptionMessages.get(UNEXPECTED_VALUE), fieldName) + ". Field names start with a dot (.)" );
				//log.debug("---------->  ------> OBJECT-VALUES: OK field-name: "+fieldName);
				// Consume next colon
				checkToken(st, COLON);
				
				// get field value
				//log.debug("---------->  ------> OBJECT-VALUES: BEFORE field-value");
				Object fieldValue = _unserializeObject(st);
				//log.debug("---------->  ------> OBJECT-VALUES: AFTER field-value: "+fieldValue);
				
				// Consume next comma
				//consumeWhitespaces(st);
				consumeComma(st);
				
				// set field value
				//log.debug("---------->  ------> OBJECT-VALUES: BEFORE SETTING field-value");
				Field field = fields.get(fieldName);
				try {
					//boolean isPrivate = (modifiers & Modifier.PRIVATE) != 0;
					//boolean isProtected = (modifiers & Modifier.PROTECTED) != 0;
					//boolean isPublic = (modifiers & Modifier.PUBLIC) != 0;
					boolean isAccessible = field.isAccessible();
					if (!isAccessible) field.setAccessible(true);
					field.set(object, fieldValue);
					if (!isAccessible) field.setAccessible(false);
				} catch (IllegalAccessException iae) {
					throw new RuntimeException("Could not write value of field: "+type+"."+fieldName+", value="+fieldValue, iae);
				}
				//log.debug("---------->  ------> OBJECT-VALUES: AFTER SETTING field-value");
			}
			
			// Consume object's closing tag 
			//log.debug("---------->  ------> OBJECT-VALUES: BEFORE OBJECT_CLOSE");
			consumeWhitespaces(st);
			checkToken(st, OBJECT_CLOSE);
			//log.debug("---------->  ------> OBJECT-VALUES: AFTER OBJECT_CLOSE");
			// Consume any trailing comma
			consumeComma(st);
		}
		
		return object;
	}
	
	protected Date fromW3cFormat(String value) {
		try {
			if (value.endsWith(":00")) value = value.substring(0, value.length()-2);
			return JsonSerializer.w3cFmt.parse(value);
		} catch (java.text.ParseException e) {
			throw new RuntimeException( String.format( exceptionMessages.get(UNEXPECTED_VALUE), value), e );
		}
	}
	
	protected Map<String,Field> getObjectFields(Class clazz, Map<String,Field> map) {
		Class superClazz = clazz.getSuperclass();
		if(superClazz != null){
			getObjectFields(superClazz, map);
		}
		for (Field f : clazz.getDeclaredFields()) {
			map.put(f.getName(), f);
		}
		return map;
	}
	
	protected String unjsStr(String s) {
		s = (s.startsWith("\"") && s.endsWith("\""))
					? s.substring(1, s.length()-1)
					: s;
		s = StringUtils.replaceEachRepeatedly(s, JsonSerializer.DELIMETER_REPLACEMENTS, JsonSerializer.DELIMETERS);
		s = StringEscapeUtils.unescapeEcmaScript(s);
		return s;
	}
}
