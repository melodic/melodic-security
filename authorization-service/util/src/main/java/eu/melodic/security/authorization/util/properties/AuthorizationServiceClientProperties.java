/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.util.properties;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@ToString(exclude="jwtSecret")
@Validated
@Configuration
@ConfigurationProperties
@PropertySource("file:${MELODIC_CONFIG_DIR}/authorization-client.properties")
@Slf4j
public class AuthorizationServiceClientProperties {
	
	@Valid @NotNull
	private PdpConfig pdp;
	@Valid
	private DataProperties data;
	@Valid
	private ContextClientProperties context;
	@Valid
	private String jwtSecret;

	// --------------------------------------------------------------
	
	@Data
	@ToString(exclude="accessKey")
	public static class PdpConfig {
		@Valid
		private boolean disabled;
		@Valid @NotEmpty
		private String[] endpoints;
		@Valid
		private String loadBalanceMethod;
		@Valid
		private int retryCount;
		@Valid
		private String accessKey;
		@Valid
		private HttpClientProperties httpClient;
	}
	
	// --------------------------------------------------------------
	
	@Data
	public static class DataProperties {
		@Valid
		private String[] extractors;
	}
	
	// --------------------------------------------------------------
	
	public static boolean booleanValue(String str) {
		return booleanValue(str, false);
	}
	
	public static boolean booleanValue(String str, boolean defVal) {
		if (str==null || (str=str.trim()).isEmpty()) return defVal;
		return (str.equalsIgnoreCase("true") || str.equalsIgnoreCase("yes") || str.equalsIgnoreCase("on"));
	}
}
