/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.util;

//import com.fasterxml.jackson.annotation.JsonIgnore;
//import com.fasterxml.jackson.annotation.JsonInclude;
//import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.Map;

//@JsonInclude(JsonInclude.Include.NON_NULL)
@Data @NoArgsConstructor
public class CheckAccessResponse {
	@NonNull //@JsonProperty("responseId")
	private String id;
	@NonNull //@JsonProperty("responseData")
	private String content;
	@NonNull //@JsonProperty("responseTimestamp")
	private long timestamp = System.currentTimeMillis();
	//@JsonProperty("xacml-response")
	private Map<String,Object> xacmlResponse;
}
