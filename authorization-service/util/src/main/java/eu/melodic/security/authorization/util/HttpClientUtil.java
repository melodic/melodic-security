/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.util;

import eu.melodic.security.authorization.util.properties.HttpClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContextBuilder;
import org.apache.http.impl.client.HttpClients;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.security.KeyStore;

@Slf4j
public class HttpClientUtil {
	
	public static RestTemplate createRestTemplate(HttpClientProperties properties, String endpoint) throws Exception {
		// if 'endpoint' starts with 'https', then use SSL, else use default http client
		boolean useSsl = endpoint.substring(0,6).toLowerCase().startsWith("https:");
		return createRestTemplate(properties, useSsl);
	}
	
	public static RestTemplate createRestTemplate(HttpClientProperties properties, boolean useSsl) throws Exception {
		// Get http client configuration
		String trustStoreFile = properties.getKeystoreFile();
		String trustStoreFileType = properties.getKeystoreFileType();
		String trustStorePassword = properties.getKeystorePassword();
		
		trustStoreFile = trustStoreFile!=null ? trustStoreFile.trim() : "";
		trustStoreFileType = trustStoreFileType!=null ? trustStoreFileType.trim() : "";
		trustStorePassword = trustStorePassword!=null ? trustStorePassword.trim() : "";
		
		// if 'useSsl' is false or truststore file is empty, then we return restTemplate with default http client
		if (! useSsl || trustStoreFile.isEmpty()) {
			// Plain HTTP is assumed since we don't have an https PDP endpoint or a keystore file
			log.debug("** RestTemplate initialized without HTTPS");
			return new RestTemplate();
		}
		
		// if 'useSsl' is true then an HTTPS client will be used
		log.info("Loading truststore from: {}", trustStoreFile);
		KeyStore trustStore = KeyStore.getInstance(trustStoreFileType);	// KeyStore.getDefaultType() returns "JKS"
		java.io.FileInputStream instream = new java.io.FileInputStream(new java.io.File(trustStoreFile));
		try {
			trustStore.load(instream, trustStorePassword.toCharArray());
		} finally {
			instream.close();
		}
		
		javax.net.ssl.SSLContext sslContext = 
			new SSLContextBuilder()
			//or SSLContexts.custom()
				.loadTrustMaterial(trustStore)
				//.loadTrustMaterial(trustStore, new TrustSelfSignedStrategy())
				//.loadKeyMaterial(keyStore, keyStorePassword.toCharArray())
				.build();
		
		//log.debug("Source file: "+ org.apache.http.conn.ssl.SSLConnectionSocketFactory.class.getProtectionDomain().getCodeSource().getLocation().getPath() );
		
		SSLConnectionSocketFactory socketFactory = new SSLConnectionSocketFactory(
				sslContext,
				NoopHostnameVerifier.INSTANCE	// allow all hosts  (see also: BrowserCompatHostnameVerifier, DefaultHostnameVerifier)
		);
		
		HttpClient httpClient = HttpClients.custom().setSSLSocketFactory(socketFactory).build();
		ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);
		
		log.debug("** RestTemplate initialized with HTTPS");
		return new RestTemplate(requestFactory);
	}
}