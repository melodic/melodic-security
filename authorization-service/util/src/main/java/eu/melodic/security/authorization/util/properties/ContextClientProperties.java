/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.util.properties;

import lombok.Data;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Data
@Validated
@ToString(exclude="accessKey")
@Configuration
@Slf4j
public class ContextClientProperties {
	@Valid
	private boolean enable = false;
	@Valid
	private String repositoryClientClass;
	@Valid
	private List<String> extractorClasses;
	@Valid
	private String repositoryUrl;
	@Valid
	private String accessKey;
	
	@Valid
	private HttpClientProperties httpClient;
}
