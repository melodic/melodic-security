/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client.example;

import eu.melodic.security.authorization.context.ContextElement;
import eu.melodic.security.authorization.context.extractor.HttpRequestContextExtractor;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
public class MyHttpRequestContextExtractor extends HttpRequestContextExtractor {
	@Override
	protected void doContextExtraction(HttpServletRequest request, List<ContextElement> context) {
		context.add(new ContextElement<String>("my-test-context", "my-test-context---"+java.util.UUID.randomUUID()));
		log.info(">>>  ADDING TEST CONTEXT at CLIENT-SIDE...\n {}", context);
	}
}