/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client.example;

import eu.melodic.security.authorization.client.AuthorizationRequired;
import org.springframework.stereotype.Repository;

@Repository
public class Dao {
	@AuthorizationRequired
	public String retrieveSomething() {
		//return "Dao";
		return "Dao!!!";
	}
	
	@AuthorizationRequired
	public String updateSomething(String a, int b) {
		//return "UPDATE Dao";
		return "UPDATE Dao!!!";
	}
}