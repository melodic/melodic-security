/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.client.example;

import eu.melodic.security.authorization.client.AuthorizationRequired;
import eu.melodic.security.authorization.client.extractor.DataExtractorHelper;
import eu.melodic.security.authorization.util.properties.AuthorizationServiceClientProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.BadRequestException;
import java.util.Map;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Slf4j
@RestController
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class ExampleClientController {
	private AuthorizationServiceClientProperties clientProperties;

	@RequestMapping(value = "/", method = GET)
	public String index() {
		return test();
	}
	
	@RequestMapping(value = {"/test/**","/test/no/**"}, method = GET)
	@AuthorizationRequired
	public String test() {
		log.info("**********  REQUEST RECEIVED **********");
		return "Test OK";
	}
	
	@RequestMapping(value = "/test2/**", method = GET)
	// Spring-Boot Tomcat Authorization Interceptor is configured to check calls to this method
	public String test2() {
		log.info("**********  REQUEST RECEIVED **********");
		return "Test-2 OK";
	}
	
	@RequestMapping(value = "/dao", method = GET)
	public String testDao() {
		log.info("**********  REQUEST RECEIVED - DAO **********");
		Dao dao = new Dao();
		String value = dao.retrieveSomething();
		return format("{ \"message\": \"%s\" }", value);
	}
	
	@RequestMapping(value = "/preauth", method = GET)
	public String preauth() {
		log.info("**********  REQUEST RECEIVED - preauth **********");
		//log.info(">>> {}", clientProperties);
		DataExtractorHelper helper = new DataExtractorHelper(clientProperties);
		Map<String,Object> data = helper.getModelData(null);
		log.info("Extracted data: {}", data);
		return "Preauth OK\n"+data;
	}
	
	@ExceptionHandler
	@ResponseStatus(BAD_REQUEST)
	public String handleException(BadRequestException exception) {
		log.error(format("Returning error response: invalid request (%s) ", exception.getMessage()));
		return exception.getMessage();
	}
}
