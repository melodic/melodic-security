<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
  ~
  ~ This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
  ~ If a copy of the MPL was not distributed with this file, You can obtain one at
  ~ https://www.mozilla.org/en-US/MPL/2.0/
  -->
<xacml3:Policy xmlns:xacml3="urn:oasis:names:tc:xacml:3.0:core:schema:wd-17" PolicyId="TC-AC-07+08+09" RuleCombiningAlgId="urn:oasis:names:tc:xacml:1.0:rule-combining-algorithm:first-applicable" Version="1.0">
	<xacml3:Description>
		Sample policy for CAS use case:
		- VM deployments only within Europe or DE
		- VM deployments only during working days (= Monday-Friday) and between 08:00 and 17:00 CET
	</xacml3:Description>
	
	<xacml3:Target>
		<xacml3:AnyOf>
			<xacml3:AllOf>
				<!-- If subject is "Adapter"... -->
				<xacml3:Match MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
					<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">Adapter</xacml3:AttributeValue>
					<xacml3:AttributeDesignator AttributeId="user.identifier" Category="urn:oasis:names:tc:xacml:1.0:subject-category:access-subject" DataType="http://www.w3.org/2001/XMLSchema#string" MustBePresent="true"/>
				</xacml3:Match>
				<!-- ...and if action is "DEPLOY"... -->
				<xacml3:Match MatchId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
					<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">DEPLOY</xacml3:AttributeValue>
					<xacml3:AttributeDesignator AttributeId="actionId" Category="urn:oasis:names:tc:xacml:3.0:attribute-category:action" DataType="http://www.w3.org/2001/XMLSchema#string" MustBePresent="true"/>
				</xacml3:Match>
			</xacml3:AllOf>
		</xacml3:AnyOf>
	</xacml3:Target>
	
	<!-- ...then use this policy to Preauthorize a deployment instance model -->
	
	<xacml3:Rule Effect="Permit" RuleId="Rule-000000">
		<xacml3:Condition>
				
			<!-- ... check if all VMs' locations are DE, weekday is between Mon and Fri
					and current date/time is between 08:00 and 17:00 CET -->
			<xacml3:Apply FunctionId="urn:oasis:names:tc:xacml:1.0:function:and">
			
					<!-- ... location bag contains a single value -->
					<xacml3:Apply FunctionId="urn:oasis:names:tc:xacml:1.0:function:integer-equal">
						<xacml3:Apply FunctionId="urn:oasis:names:tc:xacml:1.0:function:string-bag-size" >
							<xacml3:AttributeDesignator 
								AttributeId="all-vm-countries"
								DataType="http://www.w3.org/2001/XMLSchema#string"
								Category="urn:oasis:names:tc:xacml:3.0:attribute-category:environment"
								MustBePresent="true"
							/>
						</xacml3:Apply>
						<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#integer">1</xacml3:AttributeValue>
					</xacml3:Apply>
					
					<!-- ... location bag single value equals to 'DE' -->
					<xacml3:Apply FunctionId="urn:oasis:names:tc:xacml:1.0:function:string-equal">
						<xacml3:Apply FunctionId="urn:oasis:names:tc:xacml:1.0:function:string-one-and-only">
							<xacml3:AttributeDesignator 
								AttributeId="all-vm-countries"
								DataType="http://www.w3.org/2001/XMLSchema#string"
								Category="urn:oasis:names:tc:xacml:3.0:attribute-category:environment"
								MustBePresent="true"
							/>
						</xacml3:Apply>
						<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">DE</xacml3:AttributeValue>
					</xacml3:Apply>
					
					<!-- ... current week day is between Monday and Friday -->
					<xacml3:Apply FunctionId="urn:oasis:names:tc:xacml:1.0:function:string-at-least-one-member-of">
						<xacml3:Apply FunctionId="urn:oasis:names:tc:xacml:1.0:function:string-bag">
							<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">Monday</xacml3:AttributeValue>
							<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">Tuesday</xacml3:AttributeValue>
							<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">Wednesday</xacml3:AttributeValue>
							<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">Thursday</xacml3:AttributeValue>
							<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#string">Friday</xacml3:AttributeValue>
						</xacml3:Apply>
						<xacml3:AttributeDesignator
								AttributeId="current-weekday"
								DataType="http://www.w3.org/2001/XMLSchema#string"
								Category="urn:oasis:names:tc:xacml:3.0:attribute-category:environment"
								MustBePresent="true"
						/>
					</xacml3:Apply>

					<!-- ... and current time is between 08:00 and 17:00 CET -->
					<xacml3:Apply FunctionId="urn:oasis:names:tc:xacml:2.0:function:time-in-range">
						<xacml3:Apply FunctionId="urn:oasis:names:tc:xacml:1.0:function:time-one-and-only" >
							<xacml3:AttributeDesignator
									AttributeId="urn:oasis:names:tc:xacml:1.0:environment:current-time"
									DataType="http://www.w3.org/2001/XMLSchema#time"
									Category="urn:oasis:names:tc:xacml:3.0:attribute-category:environment"
									MustBePresent="true"
							/>
						</xacml3:Apply>
						<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#time">08:00:00+01:00</xacml3:AttributeValue>
						<xacml3:AttributeValue DataType="http://www.w3.org/2001/XMLSchema#time">17:00:00+01:00</xacml3:AttributeValue>
					</xacml3:Apply>

			</xacml3:Apply>
				
		</xacml3:Condition>
	</xacml3:Rule>
	
	<xacml3:Rule Effect="Deny" RuleId="Deny-Rule"/>
	
</xacml3:Policy>
