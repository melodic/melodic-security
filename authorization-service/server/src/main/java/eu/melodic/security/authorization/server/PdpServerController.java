/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.server;

import eu.melodic.security.authorization.context.ContextClientUtil;
import eu.melodic.security.authorization.context.ContextExtractor;
import eu.melodic.security.authorization.context.ContextRepositoryClient;
import eu.melodic.security.authorization.server.pdp.AuthorizationServicePDP;
import eu.melodic.security.authorization.server.properties.AuthorizationServiceProperties;
import eu.melodic.security.authorization.server.util.AttributeUtil;
import eu.melodic.security.authorization.util.CheckAccessRequest;
import eu.melodic.security.authorization.util.CheckAccessResponse;
import eu.melodic.security.authorization.util.json.JsonSerializer;
import eu.melodic.security.authorization.util.json.JsonUnserializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.BadRequestException;
import java.util.*;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Slf4j
@RestController
//@AllArgsConstructor	(onConstructor = @__({@Autowired}))
public class PdpServerController implements ApplicationContextAware {
	
	private ContextExtractor extractor;
	private AuthorizationServicePDP pdp;
	private AuthorizationServiceProperties properties;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		log.debug("Setting application context");
		this.pdp = applicationContext.getBean(AuthorizationServicePDP.class);
		this.properties = applicationContext.getBean(AuthorizationServiceProperties.class);
		log.info("Authorization Service configuration: {}", properties);
		
		// Configure request context extraction
		extractor = ContextClientUtil.createExtractor(properties.getRequestContext());
		log.info("Server-side request context extraction: {}", extractor!=null);
	}
	
	@RequestMapping(value = "/checkJsonAccessRequest", method = POST)
	public String checkJsonAccessRequest(@RequestBody String jsonRequest) {
		if (!properties.getPdp().isEnable()) throw new RuntimeException("PDP server is not enabled");
		
		// Unserialize access request from json message
		log.debug("JSON message received: {}", jsonRequest);
		CheckAccessRequest request = null;
		try { request = (CheckAccessRequest) new JsonUnserializer().unserializeObject(jsonRequest); } 
		catch (Exception e) { log.error("Unserialization failed: original JSON message:\n{}\nException: ", jsonRequest, e); throw e; }
		log.trace("JSON message unserialized to request object: {}", request);
		
		// Extract request context
		long requestContextId = -1;
		String val = Objects.toString( request.getXacmlRequest().get("request-id"), "-1" );
		try { requestContextId = Long.parseLong( val==null ? "-1" : val ); } catch (Exception ex) {}
		log.debug("XACML Request contains request-id (used in storing context): {}", requestContextId);
		if (extractor!=null) requestContextId = extractor.extractContextForRequest(requestContextId, request);
		log.info("Request context extracted");
		
		// Get information from request
		String requestId = request.getId();
		log.info("Received Access Request (JSON): {}", request);
		
		// Check access request
		String xacmlRequest = createXacmlRequest( request.getXacmlRequest() );
		log.debug("XACML Request: {}", xacmlRequest);
		AuthorizationServicePDP.PDP_RESULT decision = pdp.evaluateRequest( xacmlRequest );
		
		// Prepare and return response
		CheckAccessResponse response = new CheckAccessResponse();
		response.setId(requestId);
		response.setContent( decision.toString() );
		log.info("Result for Access Request evaluation: {}", response);
		
		// Clean-up request context
		if (requestContextId>=0 && extractor!=null) {
			log.debug("Clearing request context: request-id={}", requestContextId);
			ContextRepositoryClient crc = extractor.getContextRepositoryClient();
			if (crc!=null) crc.deleteContextForRequest(requestContextId);
		}
		
		// Serialize access response to a json message
		log.trace("Response object to serialize into JSON message: {}", response);
		String jsonResponse = new JsonSerializer().serializeObject(response);
		log.trace("JSON message to return: {}", jsonResponse);
		
		//return response;
		return jsonResponse;
	}
	
	//protected String createXacmlRequest(Map<String,String> requestProperties) {
	protected String createXacmlRequest(Map<String,Object> requestProperties) {
		// categorize attributes by 'attribute category'
		Map<String,List<Map.Entry<String,Object>>> categoryAttrs = new HashMap<>();
		for (Map.Entry<String,Object> entry : requestProperties.entrySet()) {
			// get attribute key and value
			String key = entry.getKey();
			Object value = entry.getValue();
			
			// get attribute category (if unknown it defaults to 'environment')
			String category = AttributeUtil.getInstance().getAttributeCategory(key, "urn:oasis:names:tc:xacml:3.0:attribute-category:environment");
			
			// store attribute entry in categoryAttrs
			List attrs = categoryAttrs.get(category);
			if (attrs==null) { attrs = new Vector(); categoryAttrs.put(category, attrs); }
			attrs.add( entry );
		}
		
		// build XACML request string
		StringBuilder buff = new StringBuilder();
		buff.append( "<xacml:Request ReturnPolicyIdList=\"true\" CombinedDecision=\"false\" xmlns:xacml=\"urn:oasis:names:tc:xacml:3.0:core:schema:wd-17\">\n" );
		for (String category : categoryAttrs.keySet()) {
			
			// open attributes category
			buff.append( format( "	<xacml:Attributes Category=\"%s\" >\n", category ) );
			
			for (Map.Entry<String,Object> entry : categoryAttrs.get(category)) {
				// get attribute key and value
				String key = entry.getKey();
				Object value = entry.getValue();
				
				// check if attribute is a Collection
				log.debug("createXacmlRequest: attribute-key={}, value-class={}", key, value!=null ? value.getClass().getName() : null);
				if (value!=null && Collection.class.isAssignableFrom(value.getClass())) {
					log.warn("createXacmlRequest: Bag will be used for attribute: {}", key);
					Collection col = (Collection)value;
					Object firstValue = null;
					java.util.Iterator it = col.iterator();
					
					// append attribute to XACML request
					buff.append( format( "		<xacml:Attribute AttributeId=\"%s\" IncludeInResult=\"true\">\n", key ) );
					while (it.hasNext()) {
						Object bagItemValue = it.next();
						
						String type = getXsdType(bagItemValue);
						if (type==null) type = AttributeUtil.getInstance().getAttributeType(key, "http://www.w3.org/2001/XMLSchema#string");
						log.warn("createXacmlRequest: Bag element value type: {}", type);
						
						String strValue = bagItemValue!=null ? bagItemValue.toString() : "";
						//if (!strValue.isEmpty())
						buff.append( format( "			<xacml:AttributeValue DataType=\"%s\">%s</xacml:AttributeValue>\n", type, strValue ) );
					}
					buff.append( format( "		</xacml:Attribute>\n" ) );
				} else 
				// get XSD attribute type (if unknown it defaults to 'xsd:string')
				{
					log.debug("createXacmlRequest: XSD single value will be used for attribute: {}", key);
					String type = getXsdType(value);
					if (type==null) type = AttributeUtil.getInstance().getAttributeType(key, "http://www.w3.org/2001/XMLSchema#string");
					log.debug("createXacmlRequest: XSD value type: {}", type);
					
					// append attribute to XACML request
					buff.append( format( "		<xacml:Attribute AttributeId=\"%s\" IncludeInResult=\"true\">\n", key ) );
					buff.append( format( "			<xacml:AttributeValue DataType=\"%s\">%s</xacml:AttributeValue>\n", type, value!=null ? value.toString() : "" ) );
					buff.append( format( "		</xacml:Attribute>\n" ) );
				}
			}
			
			// close attributes category
			buff.append( format( "	</xacml:Attributes>\n" ) );
		}
		buff.append( "</xacml:Request>" );
		
		return buff.toString();
	}
	
	protected String getXsdType(Object value) {
		if (value==null) return null;
		String typeStr = value.getClass().getName();
		if (typeStr==null) return null;
		else if (typeStr.equals("boolean")) return "http://www.w3.org/2001/XMLSchema#boolean";
		else if (typeStr.equals("byte")) return "http://www.w3.org/2001/XMLSchema#integer";
		else if (typeStr.equals("short")) return "http://www.w3.org/2001/XMLSchema#integer";
		else if (typeStr.equals("int")) return "http://www.w3.org/2001/XMLSchema#integer";
		else if (typeStr.equals("long")) return "http://www.w3.org/2001/XMLSchema#integer";
		else if (typeStr.equals("float")) return "http://www.w3.org/2001/XMLSchema#double";
		else if (typeStr.equals("double")) return "http://www.w3.org/2001/XMLSchema#double";
		else if (typeStr.equals("java.lang.Boolean")) return "http://www.w3.org/2001/XMLSchema#boolean";
		else if (typeStr.equals("java.lang.Byte")) return "http://www.w3.org/2001/XMLSchema#integer";
		else if (typeStr.equals("java.lang.Short")) return "http://www.w3.org/2001/XMLSchema#integer";
		else if (typeStr.equals("java.lang.Integer")) return "http://www.w3.org/2001/XMLSchema#integer";
		else if (typeStr.equals("java.lang.Long")) return "http://www.w3.org/2001/XMLSchema#integer";
		else if (typeStr.equals("java.lang.Float")) return "http://www.w3.org/2001/XMLSchema#double";
		else if (typeStr.equals("java.lang.Double")) return "http://www.w3.org/2001/XMLSchema#double";
		else if (typeStr.equals("java.lang.Date")) return "http://www.w3.org/2001/XMLSchema#dateTime";
		else return null;
	}
	
	@ExceptionHandler
	@ResponseStatus(BAD_REQUEST)
	public String handleException(BadRequestException exception) {
		log.error(format("Returning error response: invalid request (%s) ", exception.getMessage()));
		return exception.getMessage();
	}
}
