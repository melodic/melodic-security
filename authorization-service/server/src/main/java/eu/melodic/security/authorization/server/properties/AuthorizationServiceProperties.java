/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.server.properties;

import eu.melodic.security.authorization.util.properties.ContextClientProperties;
import eu.melodic.security.authorization.util.properties.DatabaseClientProperties;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Validated
@Configuration
@ConfigurationProperties
@Slf4j
@PropertySources({
		@PropertySource(value="file:${MELODIC_CONFIG_DIR}/authorization-server.properties")
	})
public class AuthorizationServiceProperties {

	@Valid @NotNull
	private PdpConfig pdp;
	
	@Valid
	private ContextConfig context;
	
	@Valid @NotNull
	private List<PipConfig> pip;
	
	@Valid
	private ContextClientProperties requestContext;
	
	// --------------------------------------------------------------
	
	@Data
	public static class PdpConfig {
		@Valid
		private boolean enable = true;
		@Valid 
		private String balanaConfigFile;
		@Valid
		private String policiesDir;
		@Valid
		private String policiesExtension = ".xml";
		@Valid
		private long restartPollingInterval = 1000;
	}
	
	@Data
	public static class ContextConfig {
		@Valid
		private boolean enable = true;
		@Valid
		private boolean softDelete = true;
		@Valid
		private DatabaseClientProperties db;
		@Valid
		private String contextTable = "context_elements";
		@Valid
		private String requestsTable = "requests";
	}
	
	@Data
	public static class PipConfig {
		@Valid
		private DatabaseClientProperties db;
	}
	
	// --------------------------------------------------------------
	
	public static boolean booleanValue(String str) {
		return booleanValue(str, false);
	}
	
	public static boolean booleanValue(String str, boolean defVal) {
		if (str==null || (str=str.trim()).isEmpty()) return defVal;
		return (str.equalsIgnoreCase("true") || str.equalsIgnoreCase("yes") || str.equalsIgnoreCase("on"));
	}
}
