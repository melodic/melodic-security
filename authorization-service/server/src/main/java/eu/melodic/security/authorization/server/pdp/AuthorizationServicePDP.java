/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.server.pdp;

import eu.melodic.security.authorization.server.properties.AuthorizationServiceProperties;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.wso2.balana.Balana;
import org.wso2.balana.ConfigurationStore;
import org.wso2.balana.PDP;
import org.wso2.balana.PDPConfig;
import org.wso2.balana.finder.AttributeFinder;
import org.wso2.balana.finder.AttributeFinderModule;
import org.wso2.balana.finder.PolicyFinder;
import org.wso2.balana.finder.impl.FileBasedPolicyFinderModule;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/**
 * AuthorizationServicePDP
 */
@Slf4j
@Service
public class AuthorizationServicePDP implements ApplicationContextAware {
    private AuthorizationServiceProperties properties;
    private PDP pdp;
    private DirectoryWatcher directoryWatcher;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.properties = applicationContext.getBean(AuthorizationServiceProperties.class);
        log.debug("Got configuration bean");

        initPolicyDirectoryWatcher();
    }

    protected void initPolicyDirectoryWatcher() {
        try {
            (directoryWatcher = new DirectoryWatcher()).startDirectoryWatcher(
                    properties.getPdp().getPoliciesDir(),
                    properties.getPdp().getPoliciesExtension(),
                    properties.getPdp().getRestartPollingInterval(),
                    () -> {
                        try {
                            log.warn("Restarting PDP...");
                            initPdp();
                        } catch (Exception e) {
                            log.error("PDP re-start failed", e);
                        }
                    });
        } catch (IOException e) {
            throw new BeanInitializationException("Failed to initialize Policy directory watcher", e);
        }
    }

    @PostConstruct
    public void initPdp() throws Exception {
        log.info("Initializing PDP...");

        // Get PDP configuration
        String configFileLocation = properties.getPdp().getBalanaConfigFile();
        String policyDirectoryLocation = properties.getPdp().getPoliciesDir();
        String policyFilesExtension = properties.getPdp().getPoliciesExtension();
        System.setProperty(ConfigurationStore.PDP_CONFIG_PROPERTY, configFileLocation);
        System.setProperty(FileBasedPolicyFinderModule.POLICY_DIR_PROPERTY, policyDirectoryLocation);

        // Configure policies directory (PAP)
        log.info("Getting policies files from:" + policyDirectoryLocation);
        Set<String> fileNames = getFilesInFolder(policyDirectoryLocation, policyFilesExtension);
        PolicyFinder pf = new PolicyFinder();
        FileBasedPolicyFinderModule pfm = new FileBasedPolicyFinderModule(fileNames);
        pf.setModules(Collections.singleton(pfm));
        pfm.init(pf);

        // Configure attribute finders (PIP's)
        PDPConfig pdpConfig = Balana.getInstance().getPdpConfig();
        AttributeFinder attributeFinder = pdpConfig.getAttributeFinder();
        java.util.List<AttributeFinderModule> finderModules = attributeFinder.getModules();

        // Configuring weekday attribute finder
        finderModules.add(new WeekdayAttributeFinderModule());

//XXX: EXPERIMENTAL: adding In-Memory attribute finder
        // Configuring In-memory attribute finder
        finderModules.add(new InMemoryAttributeFinderModule(null));

        // Configuring Database attribute finder(s)
        int cntPip = -1;
        for (AuthorizationServiceProperties.PipConfig pipCfg : properties.getPip()) {
            log.info("Configuring PIP #{}: {}", cntPip++, pipCfg);

            // Initialize attribute finder module from PIP configuration
            AttributeFinderModule afm = null;
            if (pipCfg.getDb() != null) {
                afm = new DBAttributeFinderModule(pipCfg.getDb(), "" + cntPip);
            }

            // Add new attribute finder module in PDP configuration
            if (afm != null) {
                finderModules.add(afm);
            } else {
                log.info("PIP configuration is not valid. Ignoring it: {}", pipCfg);
            }
        }
        attributeFinder.setModules(finderModules);

        // Initialize PDP
        pdp = new PDP(new PDPConfig(attributeFinder, pf, null, true));

        log.info("PDP initialized");
    }

    @PreDestroy
    public void cleanup() {
    }

    public PDP_RESULT evaluateRequest(String xacmlRequestStr) {
        try {
            return __evaluateRequest(xacmlRequestStr);
        } catch (Exception ex) {
            log.error("EXCEPTION while evaluating access request: ", ex);
            return PDP_RESULT.ERROR;
        }
    }

    protected PDP_RESULT __evaluateRequest(String xacmlRequest) throws IOException {
        // Evaluate access request against policies
        log.debug("XACML Request:\n{}\n", xacmlRequest);
        String xacmlResponse = pdp.evaluate(xacmlRequest);
        log.debug("XACML Response:\n{}\n", xacmlResponse);

        // Extract XACML response decision
        PDP_RESULT result;
        if (xacmlResponse.indexOf("<Decision>Permit</Decision>") > 0) result = PDP_RESULT.PERMIT;
        else if (xacmlResponse.indexOf("<Decision>Deny</Decision>") > 0) result = PDP_RESULT.DENY;
        else if (xacmlResponse.indexOf("<Decision>Indeterminate</Decision>") > 0) result = PDP_RESULT.INDETERMINATE;
        else if (xacmlResponse.indexOf("<Decision>NotApplicable</Decision>") > 0) result = PDP_RESULT.NOT_APPLICABLE;
        else result = PDP_RESULT.ERROR;

        return result;
    }

    private static Set<String> getFilesInFolder(String directory, final String extension) throws FileNotFoundException {
        File dir = new File(directory);
        String[] children = null;
        if (StringUtils.isNotEmpty(extension)) {
            FilenameFilter filter = (f, name) -> name.endsWith(StringUtils.defaultIfEmpty(extension,""));
            children = dir.list(filter);
        } else {
            children = dir.list();
        }
        if (children == null) {
        	log.error("Policies directory not found: {}", directory);
        	throw new FileNotFoundException("Policies directory not found: "+directory);
		}
        HashSet<String> result = new HashSet<String>();
        String fileSep = System.getProperty("file.separator");
        for (int i = 0; i < children.length; i++) {
            result.add(directory + fileSep + children[i]);
        }
        return result;
    }

    public enum PDP_RESULT {
        PERMIT, DENY, INDETERMINATE, NOT_APPLICABLE, ERROR
    }
}
