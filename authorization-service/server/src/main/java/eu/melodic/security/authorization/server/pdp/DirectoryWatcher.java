/*
 * Copyright (C) 2017 Institute of Communication and Computer Systems (imu.iccs.com)
 *
 * This Source Code Form is subject to the terms of the
 * Mozilla Public License, v. 2.0. If a copy of the MPL
 * was not distributed with this file, You can obtain one at
 * http://mozilla.org/MPL/2.0/.
 */

package eu.melodic.security.authorization.server.pdp;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.task.SimpleAsyncTaskExecutor;

import java.io.IOException;
import java.nio.file.*;
import java.util.concurrent.atomic.AtomicBoolean;

import static java.nio.file.StandardWatchEventKinds.*;

@Slf4j
public class DirectoryWatcher {
    private WatchService watchService;
    private SimpleAsyncTaskExecutor executor;
    private AtomicBoolean needRestart;

    DirectoryWatcher() throws IOException {
        watchService = FileSystems.getDefault().newWatchService();
        executor = new SimpleAsyncTaskExecutor();
        needRestart = new AtomicBoolean(false);
    }

    void startDirectoryWatcher(String policiesDir, String extension, long pollingInterval, Runnable callback) throws IOException {
        if (pollingInterval<=0)
            throw new IllegalArgumentException("Directory Watcher polling interval cannot be zero or negative");

        // register path with watch service
        Paths.get(policiesDir).register(watchService, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE);

        // start the PDP restart thread
        executor.execute(() -> {
            while (true) {
                try {
                    Thread.sleep(pollingInterval);
                } catch (InterruptedException e) {
                }
                if (needRestart.getAndSet(false)) {
                    // invoke callback
                    callback.run();
                }
            }
        });

        // start the policy dir. monitoring thread
        executor.execute(() -> {
            WatchKey key;
            while (true) {
                // monitor for changes in policy dir.
                try {
                    if ((key = watchService.take()) == null) continue;
                } catch (InterruptedException e) {
                    continue;
                }

                // poll events
                boolean policiesAffected = false;
                for (WatchEvent<?> event : key.pollEvents()) {
                    // filter files based on their extension
                    if (extension == null || event.context().toString().endsWith(extension)) {
                        policiesAffected = true;
                        log.info("  Policy dir. watcher: {}: {}", event.kind(), event.context());
                    }
                }
                key.reset();

                // signal PDP restart
                if (policiesAffected) {
                    needRestart.set(true);
                }
            }
        });
        log.info("Policy dir. watcher initialized");
    }
}
