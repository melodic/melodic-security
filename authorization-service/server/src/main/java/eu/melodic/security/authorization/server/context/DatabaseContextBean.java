/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.server.context;

import eu.melodic.security.authorization.context.ContextElement;
import eu.melodic.security.authorization.server.properties.AuthorizationServiceProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.sql.*;
import java.util.Collection;
import java.util.Properties;

/**
 * DatabaseContextBean
 */
@Slf4j
@Service
public class DatabaseContextBean implements ApplicationContextAware
{
	private AuthorizationServiceProperties properties;
	private Connection conn;
	private PreparedStatement insertStmt;
	private PreparedStatement insertRequestStmt;
	private PreparedStatement deleteStmt;
	private PreparedStatement deleteRequestStmt;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.properties = applicationContext.getBean(AuthorizationServiceProperties.class);
		log.debug("Got configuration bean");
	}
	
	@PostConstruct
	public void init() throws Exception {
		log.info("Initializing database connection...");
		
		// Get database connection configuration
		String url = properties.getContext().getDb().getUrl();
		String username = properties.getContext().getDb().getUsername();
		String password = properties.getContext().getDb().getPassword();
		String database = properties.getContext().getDb().getDatabase();
		boolean softDelete = properties.getContext().isSoftDelete();
		String contextTable = properties.getContext().getContextTable();
		String requestsTable = properties.getContext().getRequestsTable();
		log.info("Context Server database configuration: url={}, username={}, database={}, soft-delete={}, context-table={}, requests-table={}",
				url, username, database, softDelete, contextTable, requestsTable);
		
		// Connect to the database
		Properties connectionProps = new Properties();	  
		connectionProps.put("user", username);
		connectionProps.put("password", password);
		
		log.info("Connecting to Database: {}@{}", username, url);
		this.conn = DriverManager.getConnection(url, connectionProps);
		log.debug("Connected to Database");
		
		// Create prepared statements
		String insertRequestStmtStr = 
				"INSERT INTO `"+requestsTable+"` VALUES () ";
		this.insertRequestStmt = this.conn.prepareStatement(insertRequestStmtStr, Statement.RETURN_GENERATED_KEYS);		// or (stmt, new String[] {"id"});
		
		String insertStmtStr = 
				"INSERT INTO `"+contextTable+"` " +
				"( name, value, scope, timestamp, request_id ) " +
				"VALUES " +
				"( ?, ?, ?, ?, ? ) ";
		this.insertStmt = this.conn.prepareStatement(insertStmtStr);
		
		String deleteStmtStr = softDelete
			?
				"UPDATE `"+contextTable+"` " +
				"SET del_flag = 1 " +
				"WHERE request_id = ? " +
				"AND name = ? " +
				"AND scope = ? "
			:
				"DELETE FROM `"+contextTable+"` " +
				"WHERE request_id = ? " +
				"AND name = ? " +
				"AND scope = ? ";
		this.deleteStmt = this.conn.prepareStatement(deleteStmtStr);
		
		String deleteRequestStmtStr = softDelete
			?
				"UPDATE `"+contextTable+"` " +
				"SET del_flag = 1 " +
				"WHERE request_id = ? "
			:
				"DELETE FROM `"+contextTable+"` " +
				"WHERE request_id = ? ";
		this.deleteRequestStmt = this.conn.prepareStatement(deleteRequestStmtStr);
		
		log.info("Database connection initialized");
	}
	
	@PreDestroy
	public void cleanup() throws SQLException {
		log.info("Closing database connection...");
		
		// Disconnect from database server
		if (this.conn!=null) {
			this.conn.close();
		}
		
		log.info("Database connection closed");
	}
	
	/*
	 *  Stores context that is not specific to a request
	 */
	public void storeContext(Collection<ContextElement> context) throws SQLException {
		_do_storeContextForRequest(context, -1);
	}
	
	/*
	 *  Stores context that is specific to request with id: 'requestId'
	 *  If 'requestId' < 0 then a new request id will be generated
	 */
	public long storeContextForRequest(Collection<ContextElement> context, long requestId) throws SQLException {
		if (requestId<0) {
			requestId = _get_new_request_id();
			log.info("New request-id: {}", requestId);
		}
		return _do_storeContextForRequest(context, requestId);
	}
	
	/*
	 *  Clears context elements relating to a request
	 */
	public void deleteContextForRequest(long requestId) throws SQLException {
		log.info("Deleting context of request: request-id={}", requestId);
		if (requestId>=0) {
			synchronized (deleteRequestStmt) {
				// delete context elements for request
				deleteRequestStmt.setLong(1, requestId);
				deleteRequestStmt.executeUpdate();
			}
		}
		log.debug("Context cleared");
	}
	
	protected long _get_new_request_id() throws SQLException {
		ResultSet rs = null;
		try {
			synchronized (insertRequestStmt) {
				insertRequestStmt.executeUpdate();
				rs = insertRequestStmt.getGeneratedKeys();
			}
			//if (rs.next()) {
				rs.next();
				return rs.getLong(1);
			//}
			//throw new SQLException("Could not retrieve new request id");
		} finally {
			if (rs!=null) rs.close();
		}
	}
	
	protected long _do_storeContextForRequest(Collection<ContextElement> context, long requestId) throws SQLException {
		log.info("Storing context: request-id={}", requestId);
		if (context!=null) {
			synchronized (insertStmt) {
				for (ContextElement ce : context) {
					String name = ce.getName();
					Object value = ce.getValue();
					String scope = ce.getScope();
					
					// delete previous context element value
					deleteStmt.setLong(1, requestId);
					deleteStmt.setString(2, name);
					deleteStmt.setString(3, scope);
					
					deleteStmt.executeUpdate();
					
					// execute insert SQL stetement
					insertStmt.setString(1, name);
					insertStmt.setString(2, value!=null ? value.toString() : null);
					insertStmt.setString(3, scope!=null ? scope : ContextElement._DEFAULT_SCOPE);
					insertStmt.setLong(4, ce.getTimestamp());
					insertStmt.setLong(5, requestId>=0 ? requestId : ce.getRequestId());
					//insertStmt.setLong(5, requestId);
					
					insertStmt.executeUpdate();
				}
			}
		}
		log.debug("Context stored");
		return requestId;
	}
}
