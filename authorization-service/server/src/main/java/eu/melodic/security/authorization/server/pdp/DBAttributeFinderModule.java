/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.server.pdp;

import eu.melodic.security.authorization.util.properties.DatabaseClientProperties;
import lombok.extern.slf4j.Slf4j;
import org.wso2.balana.attr.AttributeValue;
import org.wso2.balana.attr.BagAttribute;
import org.wso2.balana.attr.StringAttribute;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.ctx.Status;
import org.wso2.balana.utils.Constants.PolicyConstants;

import java.net.URI;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Database (SQL) Attribute Finder & Context Attribute Finder module
 */
@Slf4j
public class DBAttributeFinderModule extends org.wso2.balana.finder.AttributeFinderModule {
	
	public final static URI REQUEST_ATTRIBUTE_URI = URI.create("request-id");
	public final static URI REQUEST_CATEGORY_URI = URI.create(PolicyConstants.ENVIRONMENT_CATEGORY_URI);
	public final static URI STRING_DATA_TYPE_URI = URI.create(PolicyConstants.STRING_DATA_TYPE);
	
	private String pipId;
	private String dbUser;
	private String dbPassword;
	private String dbUrl;
	
	public String dbName;
	public String dbRoleTable;
	public String dbRoleColumn;
	public String dbSubIdColumn;
	
	private Connection conn;
	private Statement stmt;
	private PreparedStatement ctxStmt;
	
	private Pattern patternQuery;
	private Pattern patternContext;
	
	/**
	 * Constructor
	 * @throws SQLException 
	 */
	public DBAttributeFinderModule(DatabaseClientProperties config, String id) throws SQLException {
		this.pipId = id;
		this.dbUrl = config.getUrl();
		this.dbUser = config.getUsername();
		this.dbPassword = config.getPassword();
		this.dbName = config.getDatabase();
		
		Properties connectionProps = new Properties();	  
		connectionProps.put("user", this.dbUser);
		connectionProps.put("password", this.dbPassword);
		
		log.info("Connecting to Database: {}@{}", dbUser, dbUrl);
		this.conn = DriverManager.getConnection(this.dbUrl, connectionProps);
		log.debug("Connected to Database");
		
		this.stmt = conn.createStatement();
		this.ctxStmt = conn.prepareStatement("SELECT `value` FROM `context_elements` WHERE del_flag = 0 AND `name` = ? AND `scope` = ? AND ( ? < 0  OR  request_id = ? ) ");
		
		this.patternQuery = Pattern.compile("urn:eu:melodic:attributes:db:(\\w+):(\\w+):(\\w+):(\\w+):(\\w+)");
		this.patternContext = Pattern.compile("urn:eu:melodic:attributes:context:(\\w+):(\\w+):(\\w+)");
	}
	
	@Override
	public boolean isDesignatorSupported() {
		return true;
	}
	
	public void close() throws SQLException {
		log.info("Disconnecting from Database: {}@{}", dbUser, dbUrl);
		this.conn.close();
		log.debug("Disconnected from Database");
	}
	
	@Override
	public EvaluationResult findAttribute(URI attributeType, URI attributeId, 
			String issuer, URI category, EvaluationCtx context) 
	{
		log.debug("Arguments: attr-type: {}, attr-uri: {}, issuer: {}, category: {}, context: {}", 
				attributeType, attributeId, issuer, category, context);
		
		if (attributeId.toString().startsWith("urn:eu:melodic:attributes:db:")) {
			log.debug("Attribute id starts with 'urn:eu:melodic:attributes:db:' and will be served by this PIP");
			log.debug("Calling findAttributeWithDbQuery...");
			return findAttributeWithDbQuery(attributeType, attributeId, issuer, category, context);
		} else
		if (attributeId.toString().startsWith("urn:eu:melodic:attributes:context:")) {
			log.debug("Attribute id starts with 'urn:eu:melodic:attributes:context:' and will be served by this PIP");
			log.debug("Calling findAttributeAsContext...");
			return findAttributeAsContext(attributeType, attributeId, issuer, category, context);
		} else {
			log.debug("Attribute not supported: {}", attributeId);
			return new EvaluationResult(
					BagAttribute.createEmptyBag(attributeType));
		}
	}
	
	protected EvaluationResult findAttributeWithDbQuery(URI attributeType, URI attributeId, 
			String issuer, URI category, EvaluationCtx context) 
	{
		// Check if attribute type is String
		if (!attributeType.toString().equals(PolicyConstants.STRING_DATA_TYPE)) {
			log.debug("Attribute type is NOT STRING");
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		
		// Extract arguments from attribute id
		String srcId, tableName, searchCol, valueCol, searchTerm;
		Matcher matcher = this.patternQuery.matcher( attributeId.toString() );
		if (matcher.matches() && matcher.groupCount()==5) {
			srcId = matcher.group(1).trim();
			tableName = matcher.group(2).trim();
			searchCol = matcher.group(3).trim();
			valueCol = matcher.group(4).trim();
			searchTerm = matcher.group(5).trim();
			
			log.debug("Attribute matches pattern: src-id={}, table={}, search-col={}, value-col={}, search-term={}", srcId, tableName, searchCol, valueCol, searchTerm);
		} else {
			log.debug("Attribute does not match pattern: {}", attributeId);
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		
		// Check if srcId refers to this PIP
		if (!pipId.equals(srcId)) {
			log.debug("Attribute does not refer to this PIP: src-id={}, pip-id={}", attributeId, pipId);
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		log.debug("Attribute refers to this PIP: src-id={}, pip-id={}", attributeId, pipId);
		
		// Query database
		String value = null;
		try {
			String query = String.format("SELECT `%s` FROM `%s` WHERE `%s` = '%s'", valueCol, tableName, searchCol, searchTerm);
			log.debug("Query: {}", query);
			
			ResultSet rs = stmt.executeQuery(query);
			log.debug("Queried database");
			
			if (rs.next()) {
				log.debug("Found result");
				value = rs.getString(valueCol);
				log.debug("Value: {}", value);
				
				if (rs.next()) log.warn("More than 1 values found for attribute. Returing first occurrence: attr={}", attributeId);
			} else {
				log.debug("No result");
				return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
			}
			
		} catch (SQLException ex) {
			log.error("Error while querying database", ex);
			return new EvaluationResult( new Status( Collections.singletonList(Status.STATUS_PROCESSING_ERROR), ex.getMessage() ) );
		}
		
		// Return result
		List<AttributeValue> bag = new ArrayList<AttributeValue>();
		bag.add(new StringAttribute( value ));
		
		log.debug("Returning result bag: {}", bag);
		return new EvaluationResult(new BagAttribute(URI.create(PolicyConstants.STRING_DATA_TYPE), bag));
	}
	
	protected EvaluationResult findAttributeAsContext(URI attributeType, URI attributeId, 
			String issuer, URI category, EvaluationCtx context) 
	{
		// Check if attribute type is String
		if (!attributeType.toString().equals(PolicyConstants.STRING_DATA_TYPE)) {
			log.debug("Attribute type is NOT STRING");
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		
		// Extract request-id from original XACML request
		long requestId = getRequestIdFromContext(context);
		log.debug("Request-id extracted from evaluation context: {}", requestId);
		
		// Extract arguments from attribute id
		String srcId, attrName, attrScope;
		Matcher matcher = this.patternContext.matcher( attributeId.toString() );
		if (matcher.matches() && matcher.groupCount()==3) {
			srcId = matcher.group(1).trim();
			attrName = matcher.group(2).trim();
			attrScope = matcher.group(3).trim();
			
			log.debug("Attribute matches pattern: src-id={}, attr-name={}, scope={}", srcId, attrName, attrScope);
		} else {
			log.debug("Attribute does not match pattern: {}", attributeId);
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		
		// Check if srcId refers to this PIP
		if (!pipId.equals(srcId)) {
			log.debug("Attribute does not refer to this PIP: src-id={}, pip-id={}", attributeId, pipId);
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		log.debug("Attribute refers to this PIP: src-id={}, pip-id={}", attributeId, pipId);
		
		// Query database
		String value = null;
		try {
			//String query = String.format("SELECT `value` FROM `context_elements` WHERE del_flag = 0 AND `name` = '%s' AND `scope` = '%s' AND (%d < 0  OR  request_id = %d) ", attrName, attrScope, requestId, requestId);
			//log.debug("Query: {}", query);
			//ResultSet rs = stmt.executeQuery(query);
			
			ctxStmt.setString(1, attrName);
			ctxStmt.setString(2, attrScope);
			ctxStmt.setLong(3, requestId);
			ctxStmt.setLong(4, requestId);
			
			ResultSet rs = ctxStmt.executeQuery();
			log.debug("Queried database");
			
			if (rs.next()) {
				log.debug("Found result");
				value = rs.getString(1);
				log.debug("Value: {}", value);
				
				if (rs.next()) log.warn("More than 1 values found for attribute. Returing first occurrence: attr={}", attributeId);
			} else {
				log.debug("No result");
				return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
			}
			
		} catch (SQLException ex) {
			log.error("Error while querying database", ex);
			return new EvaluationResult( new Status( Collections.singletonList(Status.STATUS_PROCESSING_ERROR), ex.getMessage() ) );
		}
		
		// Return result
		List<AttributeValue> bag = new ArrayList<AttributeValue>();
		bag.add(new StringAttribute( value ));
		
		log.debug("Returning result bag: {}", bag);
		return new EvaluationResult(new BagAttribute(URI.create(PolicyConstants.STRING_DATA_TYPE), bag));
	}
	
	protected long getRequestIdFromContext(EvaluationCtx context) 
	{
		if (context!=null) {
			org.wso2.balana.ctx.AbstractRequestCtx  arqCtx = context.getRequestCtx();
			if (arqCtx!=null) {
				java.util.Set<org.wso2.balana.xacml3.Attributes> reqAttrs = arqCtx.getAttributesSet();
				for (org.wso2.balana.xacml3.Attributes att : reqAttrs) {
					//log.warn(">>>>>>>>>>>>>   --- elem : id={}, cat={}, cont={}, sub-attrs={}", att.getId(), att.getCategory(), att.getContent(), att.getAttributes());
					if (att.getCategory().equals(REQUEST_CATEGORY_URI)) {
						java.util.Set<org.wso2.balana.ctx.Attribute> subAttrs = att.getAttributes();
						for (org.wso2.balana.ctx.Attribute subAtt : subAttrs) {
							if (subAtt!=null && subAtt.getId().equals(REQUEST_ATTRIBUTE_URI) && subAtt.getType().equals(STRING_DATA_TYPE_URI)) {
								//log.warn(">>>>>>>>>>>>>   --- > --- sub-elem : id={}, type={}, val={}", subAtt.getId(), subAtt.getType(), subAtt.getValues());
								AttributeValue value = subAtt.getValue();
								//log.warn("------------>   is-bag={}, val={}", value.isBag(), value);
								if (value!=null && !value.isBag() && value instanceof StringAttribute) {
									String valStr = ((StringAttribute)value).getValue();
									//log.warn(">>>>>>>>>>>>>   val-str: {}", valStr);
									try {
										long requestId = Long.parseLong(valStr);
										//log.warn(">>>>>>>>>>>>>   request-id: {}", requestId);
										return requestId;
									} catch (NumberFormatException nfex) {}
								}
							}
						}
					}
				}
			}
		}
		return -1;
	}
}
