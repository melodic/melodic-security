/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.server;

import eu.melodic.security.authorization.context.ContextElement;
import eu.melodic.security.authorization.server.context.DatabaseContextBean;
import eu.melodic.security.authorization.server.properties.AuthorizationServiceProperties;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.BadRequestException;
import java.util.Collection;

import static java.lang.String.format;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Slf4j
@RestController
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class ContextServerController implements ApplicationContextAware {

	private DatabaseContextBean contextBean;
	private AuthorizationServiceProperties properties;
	
	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		log.debug("Setting application context");
		this.contextBean = applicationContext.getBean(DatabaseContextBean.class);
		this.properties = applicationContext.getBean(AuthorizationServiceProperties.class);
		log.info("Authorization Service configuration: {}", properties);
	}
	
	@RequestMapping(value = "/context", method = PUT, consumes = "application/json")
	public void addContext(@RequestBody Collection<ContextElement> context) throws Exception {
		if (!properties.getContext().isEnable()) throw new RuntimeException("Context server is not enabled");
		
		log.debug("Context: {}", context);
		contextBean.storeContext(context);
		log.info("Context stored");
	}
	
	@RequestMapping(value = "/context", method = POST, consumes = "application/json")
	public long addContextForNewRequest(@RequestBody Collection<ContextElement> context) throws Exception {
		if (!properties.getContext().isEnable()) throw new RuntimeException("Context server is not enabled");
		
		log.debug("Context: {}", context);
		long newRequestId = contextBean.storeContextForRequest(context, -1);
		log.info("Context stored. New request-id={}", newRequestId);
		return newRequestId;
	}
	
	@RequestMapping(value = "/context/{request-id}", method = POST, consumes = "application/json")
	public long addContextForRequest(@RequestBody Collection<ContextElement> context, @PathVariable("request-id") long requestId) throws Exception {
		if (!properties.getContext().isEnable()) throw new RuntimeException("Context server is not enabled");
		
		log.info("Adding context for Request-id: {}", requestId);
		log.debug("Context: {}", context);
		requestId = contextBean.storeContextForRequest(context, requestId);
		log.debug("Context stored. Request-id={}", requestId);
		return requestId;
	}
	
	@RequestMapping(value = "/context/{request-id}", method = DELETE, consumes = "*/*")
	public void deleteContextForRequest(@PathVariable("request-id") long requestId) throws Exception {
		if (!properties.getContext().isEnable()) throw new RuntimeException("Context server is not enabled");
		
		log.info("Clearing context for Request-id: {}", requestId);
		contextBean.deleteContextForRequest(requestId);
		log.debug("Context cleared");
	}
	
	@ExceptionHandler
	@ResponseStatus(BAD_REQUEST)
	public String handleException(BadRequestException exception) {
		if (!properties.getContext().isEnable()) throw new RuntimeException("Context server is not enabled");
		
		log.error(format("Returning error response: invalid request (%s) ", exception.getMessage()));
		return exception.getMessage();
	}
}
