/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.server.pdp;

import lombok.extern.slf4j.Slf4j;
import org.wso2.balana.attr.AttributeValue;
import org.wso2.balana.attr.BagAttribute;
import org.wso2.balana.attr.StringAttribute;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;
import org.wso2.balana.utils.Constants.PolicyConstants;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * In-memory Attribute Finder (backed by HashMap structure)
 */
@Slf4j
public class InMemoryAttributeFinderModule extends org.wso2.balana.finder.AttributeFinderModule {
	
	public final static URI REQUEST_ATTRIBUTE_URI = URI.create("request-id");
	public final static URI REQUEST_CATEGORY_URI = URI.create(PolicyConstants.ENVIRONMENT_CATEGORY_URI);
	public final static URI STRING_DATA_TYPE_URI = URI.create(PolicyConstants.STRING_DATA_TYPE);
	
	private static Map<String,Map<String,Map<String,String>>> contextSets = new HashMap<String,Map<String,Map<String,String>>>();
	
	public static void putContextSet(String id, Map<String,Map<String,String>> contextSet) {
		contextSets.put(id, contextSet);
	}
	
	public static void removeContextSet(String id) {
		contextSets.remove(id);
	}
	
	
	private String pipId;
	private Pattern patternContext;
	
	/**
	 * Constructor
	 */
	public InMemoryAttributeFinderModule(String id) {
		this.pipId = id;
		this.patternContext = Pattern.compile("urn:eu:melodic:attributes:context:(\\w+):(\\w+)");
	}
	
	@Override
	public boolean isDesignatorSupported() {
		return true;
	}
	
	public void close() {
	}
	
	@Override
	public EvaluationResult findAttribute(URI attributeType, URI attributeId, 
			String issuer, URI category, EvaluationCtx context) 
	{
		log.debug("Arguments: attr-type: {}, attr-uri: {}, issuer: {}, category: {}, context: {}", 
				attributeType, attributeId, issuer, category, context);
		
		// Check if we support this attribute URN prefix
		if (!attributeId.toString().startsWith("urn:eu:melodic:attributes:context:")) {
			log.debug("Attribute not supported: {}", attributeId);
			return new EvaluationResult(
					BagAttribute.createEmptyBag(attributeType));
		}
		
		// Check if attribute type is String
		if (!attributeType.toString().equals(PolicyConstants.STRING_DATA_TYPE)) {
			log.debug("Attribute type is NOT STRING");
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		
		// Extract request-id from original XACML request
		long requestId = getRequestIdFromContext(context);
		log.debug("Request-id extracted from evaluation context: {}", requestId);
		
		// Get relevant context set
		Map<String,Map<String,String>> cset = contextSets.get(requestId);
		if (cset==null) cset = contextSets.get(null);	// default context set if any
		if (cset==null) {
			log.debug("No context set found for request-id={}  and no default context set exist", requestId);
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		
		// Extract arguments from attribute id
		String attrName, attrScope;
		Matcher matcher = this.patternContext.matcher( attributeId.toString() );
		if (matcher.matches() && matcher.groupCount()==2) {
			attrName = matcher.group(1).trim();
			attrScope = matcher.group(2).trim();
			
			log.debug("Attribute matches pattern: attr-name={}, scope={}", attrName, attrScope);
		} else {
			log.debug("Attribute does not match pattern: {}", attributeId);
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		
		// Retrieve scope (if specified)
		Map<String,String> scope = cset.get(attrScope);
		if (scope==null) scope = cset.get(null);
		if (scope==null) {
			log.debug("Scope not found: {}", attrScope);
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		
		// Get context value for attrName
		String value = scope.get(attrName);
		if (value==null) {
			log.debug("No result for attribute: {}", attrName);
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}
		log.debug("Value: {}", value);
		
		// Return result
		List<AttributeValue> bag = new ArrayList<AttributeValue>();
		bag.add(new StringAttribute( value ));
		
		log.debug("Returning result bag: {}", bag);
		return new EvaluationResult(new BagAttribute(URI.create(PolicyConstants.STRING_DATA_TYPE), bag));
	}
	
//XXX: DUPLICATE CODE: with "DBAttributeFinderModule"
	protected long getRequestIdFromContext(EvaluationCtx context) 
	{
		if (context!=null) {
			org.wso2.balana.ctx.AbstractRequestCtx  arqCtx = context.getRequestCtx();
			if (arqCtx!=null) {
				java.util.Set<org.wso2.balana.xacml3.Attributes> reqAttrs = arqCtx.getAttributesSet();
				for (org.wso2.balana.xacml3.Attributes att : reqAttrs) {
					//log.warn(">>>>>>>>>>>>>   --- elem : id={}, cat={}, cont={}, sub-attrs={}", att.getId(), att.getCategory(), att.getContent(), att.getAttributes());
					if (att.getCategory().equals(REQUEST_CATEGORY_URI)) {
						java.util.Set<org.wso2.balana.ctx.Attribute> subAttrs = att.getAttributes();
						for (org.wso2.balana.ctx.Attribute subAtt : subAttrs) {
							if (subAtt!=null && subAtt.getId().equals(REQUEST_ATTRIBUTE_URI) && subAtt.getType().equals(STRING_DATA_TYPE_URI)) {
								//log.warn(">>>>>>>>>>>>>   --- > --- sub-elem : id={}, type={}, val={}", subAtt.getId(), subAtt.getType(), subAtt.getValues());
								AttributeValue value = subAtt.getValue();
								//log.warn("------------>   is-bag={}, val={}", value.isBag(), value);
								if (value!=null && !value.isBag() && value instanceof StringAttribute) {
									String valStr = ((StringAttribute)value).getValue();
									//log.warn(">>>>>>>>>>>>>   val-str: {}", valStr);
									try {
										long requestId = Long.parseLong(valStr);
										//log.warn(">>>>>>>>>>>>>   request-id: {}", requestId);
										return requestId;
									} catch (NumberFormatException nfex) {}
								}
							}
						}
					}
				}
			}
		}
		return -1;
	}
}
