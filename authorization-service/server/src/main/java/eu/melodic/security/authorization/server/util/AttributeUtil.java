/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.server.util;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

@Slf4j
public class AttributeUtil {

	public static final String attributeConfigFile = "/xacml-attributes.txt";
	private static AttributeUtil instance = new AttributeUtil();
	
	private HashMap<String,String> attributeCategories;
	private HashMap<String,String> attributeTypes;
	
	public static AttributeUtil getInstance() {
		return instance;
	}
	
	protected AttributeUtil() {
		try {
			loadAttributeConfig();
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	protected void loadAttributeConfig() throws IOException {
		if (attributeCategories!=null && attributeTypes!=null) return;
		
		attributeCategories = new HashMap<String,String>();
		attributeTypes = new HashMap<String,String>();
		
		String contents = null;
		try (InputStream is = AttributeUtil.class.getResourceAsStream(attributeConfigFile)) {
			java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
			contents = s.hasNext() ? s.next() : "";
		}
		
		for (String line : contents.split("\n+")) {
			if (line.startsWith("#")) continue;
			String[] attrCfg = line.trim().split("[ \t\r]+");
			if (attrCfg.length<2) continue;
			String attribute = attrCfg[0].trim();
			if (attribute.isEmpty()) continue;
			String category = attrCfg[1].trim();
			if (category.isEmpty()) continue;
			String type = null;
			if (attrCfg.length>2) {
				type = attrCfg[2].trim();
				if (type.isEmpty()) type = "http://www.w3.org/2001/XMLSchema#string";
			} else {
				type = "http://www.w3.org/2001/XMLSchema#string";
			}
			
			attributeCategories.put(attribute, category);
			attributeTypes.put(attribute, type);
		}
		
		log.info("XACML Attribute Categories:\n{}", attributeCategories);
		log.info("XACML Attribute Data Types:\n{}", attributeTypes);
	}
	
	public String getAttributeCategory(String key, String defVal) {
		return attributeCategories.getOrDefault(key, defVal);
	}
	
	public String getAttributeType(String key, String defVal) {
		return attributeTypes.getOrDefault(key, defVal);
	}
}
