/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.server.pdp;

import lombok.extern.slf4j.Slf4j;
import org.wso2.balana.XACMLConstants;
import org.wso2.balana.attr.AttributeValue;
import org.wso2.balana.attr.BagAttribute;
import org.wso2.balana.attr.StringAttribute;
import org.wso2.balana.cond.EvaluationResult;
import org.wso2.balana.ctx.EvaluationCtx;

import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * In-memory Attribute Finder (backed by HashMap structure)
 */
@Slf4j
public class WeekdayAttributeFinderModule extends org.wso2.balana.finder.AttributeFinderModule {

	public final static String ENVIRONMENT_CURRENT_WEEKDAY = "current-weekday";

	private final SimpleDateFormat dateFormat;

	public WeekdayAttributeFinderModule() {
		dateFormat = new SimpleDateFormat("EEEE", Locale.ENGLISH);
	}

	@Override
	public boolean isDesignatorSupported() {
		return true;
	}

	@Override
	public Set<String> getSupportedCategories() {
		HashSet<String> set = new HashSet<String>();
		set.add(XACMLConstants.ENT_CATEGORY);
		return set;
	}

	public void close() {
	}
	
	@Override
	public EvaluationResult findAttribute(URI attributeType, URI attributeId, 
			String issuer, URI category, EvaluationCtx context) 
	{
		log.debug("Arguments: attr-type: {}, attr-uri: {}, issuer: {}, category: {}, context: {}",
				attributeType, attributeId, issuer, category, context);

		// check if category is environment
		if (!XACMLConstants.ENT_CATEGORY.equals(category.toString())){
			log.debug("Attribute Category is not supported: {}", category);
			return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
		}

		// get attribute name
		String attrName = attributeId.toString();
		log.debug("Attribute Name: {}", attrName);

		if (attrName.equals(ENVIRONMENT_CURRENT_WEEKDAY)) {
			// check attribute type
			if (!attributeType.toString().equals(StringAttribute.identifier)) {
				log.debug("Attribute Type does not match: {}", attributeType);
				return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
			}

			// get attribute value
			String weekday = dateFormat.format(new Date());
			log.debug("Attribute Value: {}", weekday);
			AttributeValue attrValue = new StringAttribute(weekday);

			// create the resulting bag
			List<AttributeValue> set = new ArrayList<AttributeValue>();
			set.add(attrValue);

			BagAttribute bag = new BagAttribute(attrValue.getType(), set);

			return new EvaluationResult(bag);
		}

		// unknown attribute
		log.debug("Attribute Name is not supported: {}", attrName);
		return new EvaluationResult(BagAttribute.createEmptyBag(attributeType));
	}
}