/*
 * Copyright (C) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)
 *
 * This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
 * If a copy of the MPL was not distributed with this file, You can obtain one at
 * https://www.mozilla.org/en-US/MPL/2.0/
 */

package eu.melodic.security.authorization.server;

//import eu.melodic.security.authorization.server.context.DatabaseContextBean;
//import eu.melodic.security.authorization.server.pdp.AuthorizationServicePDP;

import eu.melodic.security.authorization.server.properties.AuthorizationServiceProperties;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor(onConstructor = @__({@Autowired}))
public class ApplicationContext {
	//private AuthorizationServicePDP pdp;
	//private DatabaseContextBean contextDb;
	private AuthorizationServiceProperties properties;
}
