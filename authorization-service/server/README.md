Authorization Server
====================
This service is a Spring Boot application that can be executed standalone or inside docker container.

As an outcome of the maven build the following artifacts can be generated:
1. jar file (authorization-service-pdp-server.jar) - ALWAYS
2. docker image (depends on the profile chosen):
   1. a when used "without-docker" profile, then no docker image is created
   2. when "docker-local" profile is checked - then docker image with the Server application is being created and pushed to local docker repository
   3. when "docker-remote" profile is checked - then docker image with the Server application is being created and pushed to remote docker repository (as pointed in pom.xml file)

#### Copyright
Copyright (c) 2017-2019 Institute of Communication and Computer Systems (imu.iccs.gr)

This Source Code Form is subject to the terms of the Mozilla Public License, v2.0.
If a copy of the MPL was not distributed with this file, You can obtain one at
https://www.mozilla.org/en-US/MPL/2.0/

Launching app
-------------
#### Standalone Mode
When launching app in standalone mode the following environment variables must be provided:
 - spring.config.location=[projectdir]\src\main\resources\config\authorization-server.properties
 - MELODIC_CONFIG_DIR=[projectdir]\src\main\resources\config\
 
All of the configuration files are under config dir
 
Remark: Application requires database to store its data to. In standalone mode launch - the database needs to be up and running before the start of the Auth Server.
 
#### Docker Mode
It is possible to run the docker image that was created when docker-local or docker-remote flag was checked during the build.

The easiest way is to use the actual melodic.yml file that describes melodic services. The part regarding authorization server contains 2 docker services (mysql db and authorization server). All necessary environment variables and options are set there. There is only a need to provide valid configuration files in the directory pointed in the melodic.yml file.
For the actual melodic.yml file, please check the utils repository of melodic.
Here sample part of this file for authorization part:
~~~~
version: "3"
services:
  dbauth:
    image: mysql
    command: --default-authentication-plugin=mysql_native_password --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
    deploy:
      replicas: 1
      restart_policy:
        condition: on-failure
        max_attempts: 3
    logging:
      driver: journald
      options:
        tag: dbauth
    networks:
      inter:
        aliases:
        - melodic-dbauth
    environment:
      MYSQL_ROOT_PASSWORD: admin
      MYSQL_USER: melodic
      MYSQL_PASSWORD: melodic
      MYSQL_DATABASE: melodic_db
    ports:
    - 3308:3306
  auth-server:
    image: 88.99.85.63:5000/melodic/auth-server-rc2.0
    deploy:
      replicas: 1
      restart_policy:
        condition: on-failure
        max_attempts: 3
    logging:
      driver: journald
      options:
        tag: auth
    networks:
      inter:
        aliases:
        - melodic-auth
    volumes:
    - Conf:/config
    - Logs:/logs
    environment:
    - LOG_FILE=/logs/authserver.log
    - spring.config.location=/config/authorization-server.properties
    - MELODIC_CONFIG_DIR=/config
    - DB_HOST=melodic-dbauth
    - DB_PORT=3306
    ports:
    - 8098:8080
networks:
  inter:
    driver: overlay
    ipam:
      driver: default
      config:
      - subnet: 10.10.0.0/16
volumes:
  Conf:
  Logs:
~~~~
